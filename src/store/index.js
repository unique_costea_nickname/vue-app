import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'
import router from '../router'

import { formatSubmitData } from '../utils'

Vue.use(Vuex)

// mutations
const SIMPLE_FIELD_UPDATE = 'SIMPLE_FIELD_UPDATE'
const BUNDLE_FIELDS_UPDATE = 'BUNDLE_FIELDS_UPDATE'
const RESET_STATE = 'RESET_STATE'
const UPLOAD_FILE = 'UPLOAD_FILE'
const UPDATE_BACKEND_ERRORS = 'UPDATE_BACKEND_ERRORS'
const UPDATE_AUTH = 'UPDATE_AUTH'
const UPDATE_PAGE_NUMBER = 'UPDATE_PAGE_NUMBER'
const SET_LAST_PAGE = 'SET_LAST_PAGE'

// actions
const GET_AUTOCOMPLETE_OPTIONS = 'GET_AUTOCOMPLETE_OPTIONS'
const SUBMIT_PAGE = 'SUBMIT_PAGE'
const CHECK_EMAIL_UNIQNUESS = 'CHECK_EMAIL_UNIQNUESS'
const FETCH_LOCATION = 'FETCH_LOCATION'

const store = new Vuex.Store({
  state: {
    form: {},
    upload: [],
    routes: {
      currentPage: 0,
      lastPageIndex: 0
    }
  },

  mutations: {
    [SIMPLE_FIELD_UPDATE] (state, payload) {
      let { name, value, group } = payload

      if (group) {
        if (!state.form[group]) state.form[group] = {}

        state.form[group][name] = value
      } else {
        state.form[name] = value
      }
    },

    [BUNDLE_FIELDS_UPDATE] (state, payload) {
      payload.map(({name, value}) => {
        state.form[name] = value
      })
    },

    [UPLOAD_FILE] (state, payload) {
      let { name, file, fileName } = payload
      let newFile = {
        name: name,
        file: file,
        fileName: fileName
      }

      state.upload.push(newFile)
    },

    [RESET_STATE] (state, payload = {except: []}) {
      const { form } = state

      for (let key in form) {
        if (!payload.except.includes(key)) {
          delete form[key]
        }
      }

      Vue.set(state, 'form', form)
    },

    [UPDATE_BACKEND_ERRORS] (state, payload) {
      Vue.set(state, 'errors', payload.messages)
    },

    [UPDATE_AUTH] (state, payload) {
      Vue.set(state, 'auth', payload.value)
    },

    [UPDATE_PAGE_NUMBER] (state, payload) {
      const { number } = payload
      router.push({params: { number: number }})
      Vue.set(state.routes, 'currentPage', number)
    },

    [SET_LAST_PAGE] (state, payload) {
      Vue.set(state.routes, 'lastPageIndex', payload.number)
    }
  },

  getters: {
    getFieldByKey: (state, getters) => (key) => {
      return state.form[key]
    },

    getRoute: (state, getters) => (key) => {
      return state.routes[key]
    }
  },

  actions: {
    [GET_AUTOCOMPLETE_OPTIONS] ({commit, state}, payload) {
      return axios.get(payload.url, {
        params: {
          term: payload.term,
          type: 2,
          site_group_id: 0,
          _: Date.now()
        }
      })
    },

    [SUBMIT_PAGE] ({commit, state}, payload) {
      const { form } = state

      const fData = formatSubmitData(form)

      let data = {
        'data': {
          'type': 'partial_candidates',
          'attributes': fData
        }
      }

      data = JSON.stringify(data)

      if (payload.method === 'PATCH') {
        data = new FormData()
        Object.keys(state.form).map((key) => {
          data.append(key, state.form[key])
        })

        if (state.upload.length > 0) {
          state.upload.map((item) => {
            data.append(item.name, item.file, item.fileName)
          })
        }
      }

      return axios.request({
        method: payload.method,
        url: payload.path,
        data: data,
        responseType: 'json',
        headers: {
          'Authorization': state.auth,
          'Content-Type': 'application/vnd.api+json'
        }
      })
    },

    [CHECK_EMAIL_UNIQNUESS] ({commit, state}, payload) {
      const url = '/kandidaat/email_uniqueness_check.json'
      let params = {
        email: payload.email
      }
      return axios.get(url, {params: params}, {responseType: 'json'})
    },

    [FETCH_LOCATION] ({commit, state}) {
      const url = '/kandidaat/zipcode_check.json'
      let params = {
        zipcode: state.form['home_address.zipcode'],
        number: state.form['home_address.number']
      }

      return axios.get(url, {params: params}, {responseType: 'json'})
    }
  }
})

store.subscribe((mutation, state) => {
  if (
      mutation.type === SIMPLE_FIELD_UPDATE && (
        mutation.payload.name === 'home_address.zipcode' ||
        mutation.payload.name === 'home_address.number'
      )
    ) {
    if (state.form['home_address.zipcode'] && state.form['home_address.number']) {
      store.dispatch(FETCH_LOCATION).then((response) => {
        if (response.data && Object.keys(response.data).length) {
          store.commit('BUNDLE_FIELDS_UPDATE', [
            {
              'name': 'home_address.street',
              'value': response.data.zipcode.street
            },
            {
              'name': 'home_address.city',
              'value': response.data.zipcode.city
            }
          ])
        } else {
          // show additional fields not_nl
          store.commit('BUNDLE_FIELDS_UPDATE', [
            {
              'name': 'home_address.country_id_condtional',
              'value': 'not_nl'
            }
          ])
        }
      })
      .catch((reason) => {
        console.warn(reason)
      })
    }
  }
})

window.my_store = store
export default store
