function getHumanReadableErrorLabel(query, list) {
  if (!query || !list) {
    return false
  }

  let result = false
  for (let liKey in list) {
    let pages = list[liKey]

    for (let pKey in pages) {
      let page = pages[pKey]
      page.map(section => {
        if (section.fields) {
          section.fields.map(field => {
            if (field.name && field.name.indexOf(query) >= 0) {
              result = field.placeholder || field.label
            }
          })
        }
      })
    }
  }
  if (!result) {
    result = query
  }
  return result
}

export { getHumanReadableErrorLabel }

const formatSubmitData = data => {
  let newFormData = {}

  Object.keys(data).map(key => {
    if (key.indexOf('.') > -1) {
      const objNames = key.split('.')
      if (!newFormData[objNames[0]]) {
        newFormData[objNames[0]] = {}
      }

      newFormData[objNames[0]][objNames[1]] = data[key]
    } else {
      newFormData[key] = data[key]
    }
  })

  return newFormData
}

export { formatSubmitData }

const isJsonString = str => typeof str === 'string' && /[^,:{}[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(str)

export { isJsonString }

const deepCloneArray = array => JSON.parse(JSON.stringify(array))

export { deepCloneArray }
