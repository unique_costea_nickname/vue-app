// Check the docs for the validator here http://vee-validate.logaretm.com/
import Vue from 'vue'
import VeeValidate from 'vee-validate'

import adsValidate from './rule-alpha-dash-spaces'

// Localisations
import nl from 'vee-validate/dist/locale/nl'

// fallback validation
VeeValidate.Validator.addLocale(nl)

VeeValidate.Validator.extend('alpha_dashes_spaces', {
  getMessage: field => `The field can contain only alpha letters, dashes and spaces`,
  validate: value => adsValidate(value)
})

const validatorConfig = {
  errorBagName: 'errors', // change if property conflicts.
  fieldsBagName: 'fields',
  delay: 0,
  locale: 'nl',
  dictionary: null,
  strict: true,
  enableAutoClasses: false,
  classNames: {
    touched: 'touched', // the control has been blurred
    untouched: 'untouched', // the control hasn't been blurred
    valid: 'valid', // model is valid
    invalid: 'invalid', // model is invalid
    pristine: 'pristine', // control has not been interacted with
    dirty: 'dirty' // control has been interacted with
  },
  events: 'input|blur',
  inject: false
}

Vue.use(VeeValidate, validatorConfig)

export default validatorConfig
