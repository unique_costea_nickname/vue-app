// ADS == Alpha Dashes Spaces
const adsRule = {
  en: /^[A-Z_-\s]*$/i,
  cs: /^[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ_-\s]*$/i,
  da: /^[A-ZÆØÅ_-\s]*$/i,
  de: /^[A-ZÄÖÜß_-\s]*$/i,
  es: /^[A-ZÁÉÍÑÓÚÜ_-\s]*$/i,
  fr: /^[A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ_-\s]*$/i,
  lt: /^[A-ZĄČĘĖĮŠŲŪŽ_-\s]*$/i,
  nl: /^[A-ZÉËÏÓÖÜ_-\s]*$/i,
  hu: /^[A-ZÁÉÍÓÖŐÚÜŰ_-\s]*$/i,
  pl: /^[A-ZĄĆĘŚŁŃÓŻŹ_-\s]*$/i,
  pt: /^[A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ_-\s]*$/i,
  ru: /^[А-ЯЁ_-\s]*$/i,
  sk: /^[A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ_-\s]*$/i,
  sr: /^[A-ZČĆŽŠĐ_-\s]*$/i,
  tr: /^[A-ZÇĞİıÖŞÜ_-\s]*$/i,
  uk: /^[А-ЩЬЮЯЄІЇҐ_-\s]*$/i
}

const adsValidate = (value, [locale] = [null]) => {
  if (Array.isArray(value)) {
    return value.every(val => adsValidate(val, [locale]))
  }

  // Match at least one locale.
  if (!locale) {
    return Object.keys(adsRule).some(loc => adsRule[loc].test(value))
  }

  return (adsRule[locale] || adsRule.en).test(value)
}

export default adsValidate
