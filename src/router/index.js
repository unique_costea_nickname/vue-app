import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/page/:number',
      component: Home,
      props: true
    },
    {
      path: '/',
      redirect: '/page/0'
    }
  ]
})
