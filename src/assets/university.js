/* eslint-disable */

const Config = {
  "page": {
    "0": [
      {
        "type": "sidebar",
        "containers": [
          {
            "title": "Problemen met inschrijven?",
            "section_intro": "<p>Bel direct ons hoofdkantoor via <span class=\"text-primary bold\">020 - 653 1544</span> en vraag naar Online Support of stuur een e-mail naar: <a href=\"mailto:onlinesupport@youngcapital.nl\">onlinesupport@youngcapital.nl</a></p>"
          }
        ]
      },
      {
        "title": "Vul je gegevens in om te kunnen solliciteren",
        "title_class_list": "form_title",
        "fields": [
          {
            "name": "email",
            "label": "E-mail",
            "type": "email",
            "tooltip": "Vul hier je e-mailadres in wat je standaard gebruikt. Dit zijn bijvoorbeeld Hotmail / Gmail / Yahoo accounts.",
            "is_required": true,
            "placeholder": "E-mail",
            "validation": {
              "rules": "required|email",
              "messages": {
                "required": "Dit veld is verplicht",
                "email": "Dit e-mailadres is niet geldig, vul een geldig, werkend e-mailadres in."
              }
            }
          }
        ],
        "footer_text": "<a class='popup' id='facebooklogin' data-data-width='600' data-data-height='400' href='/candidates/auth/facebook'><span class='translation_missing' title='translation missing: nl_yc.candidates.form.show_social_signup_with_facebook'>Klik hier om je gegevens aan te vullen vanuit Facebook</span></a>"
      },
      {
        "title": "Mijn persoonsgegevens",
        "fields": [
          {
            "name": "firstname",
            "label": "Roepnaam",
            "type": "text_input",
            "is_required": true,
            "validation": {
              "rules": "required|min:2|alpha",
              "messages": {
                "required": "Dit veld is verplicht",
                "min": "Deze tekst is te kort. Deze moet uit minimaal 2 karakters bestaan.",
                "alpha": "Roepnaam mag alleen letters bevatten."
              }
            }
          },
          {
            "name": "lastname",
            "label": "Achternaam",
            "type": "text_input",
            "is_required": true,
            "validation": {
              "rules": "required|alpha",
              "messages": {
                "required": "Dit veld is verplicht",
                "alpha": "Achternaam mag alleen letters bevatten."
              }
            }
          },
          {
            "name": "is_male",
            "label": "Geslacht",
            "type": "radio",
            "is_required": true,
            "options": [
              {
                "label": "Man",
                "value": "true"
              },
              {
                "label": "Vrouw",
                "value": "false"
              }
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "Dit veld is verplicht"
              }
            }
          },
          {
            "name": "date_of_birth",
            "type": "date-picker",
            "label": "Geboortedatum",
            "is_required": true,
            "options": [
              {
                "name": "day",
                "is_required": true,
                "type": "select_input",
                "options": [
                  {
                    "label": "Dag",
                    "value": "",
                    "selected": true
                  },
                  {
                    "label": "1",
                    "value": "1"
                  },
                  {
                    "label": "2",
                    "value": "2"
                  },
                  {
                    "label": "3",
                    "value": "3"
                  },
                  {
                    "label": "4",
                    "value": "4"
                  },
                  {
                    "label": "5",
                    "value": "5"
                  },
                  {
                    "label": "6",
                    "value": "6"
                  },
                  {
                    "label": "7",
                    "value": "7"
                  },
                  {
                    "label": "8",
                    "value": "8"
                  },
                  {
                    "label": "9",
                    "value": "9"
                  },
                  {
                    "label": "10",
                    "value": "10"
                  },
                  {
                    "label": "11",
                    "value": "11"
                  },
                  {
                    "label": "12",
                    "value": "12"
                  },
                  {
                    "label": "13",
                    "value": "13"
                  },
                  {
                    "label": "14",
                    "value": "14"
                  },
                  {
                    "label": "15",
                    "value": "15"
                  },
                  {
                    "label": "16",
                    "value": "16"
                  },
                  {
                    "label": "17",
                    "value": "17"
                  },
                  {
                    "label": "18",
                    "value": "18"
                  },
                  {
                    "label": "19",
                    "value": "19"
                  },
                  {
                    "label": "20",
                    "value": "20"
                  },
                  {
                    "label": "21",
                    "value": "21"
                  },
                  {
                    "label": "22",
                    "value": "22"
                  },
                  {
                    "label": "23",
                    "value": "23"
                  },
                  {
                    "label": "24",
                    "value": "24"
                  },
                  {
                    "label": "25",
                    "value": "25"
                  },
                  {
                    "label": "26",
                    "value": "26"
                  },
                  {
                    "label": "27",
                    "value": "27"
                  },
                  {
                    "label": "28",
                    "value": "28"
                  },
                  {
                    "label": "29",
                    "value": "29"
                  },
                  {
                    "label": "30",
                    "value": "30"
                  },
                  {
                    "label": "31",
                    "value": "31"
                  }
                ],
                "validation": {
                  "rules": "required",
                  "messages": {
                    "required": "Dit veld is verplicht"
                  }
                }
              },
              {
                "name": "month",
                "is_required": true,
                "type": "select_input",
                "options": [
                  {
                    "label": "Maand",
                    "value": "",
                    "selected": true
                  },
                  {
                    "label": "Januari",
                    "value": "1"
                  },
                  {
                    "label": "Februari",
                    "value": "2"
                  },
                  {
                    "label": "Maart",
                    "value": "3"
                  },
                  {
                    "label": "April",
                    "value": "4"
                  },
                  {
                    "label": "Mei",
                    "value": "5"
                  },
                  {
                    "label": "Juni",
                    "value": "6"
                  },
                  {
                    "label": "Juli",
                    "value": "7"
                  },
                  {
                    "label": "Augustus",
                    "value": "8"
                  },
                  {
                    "label": "September",
                    "value": "9"
                  },
                  {
                    "label": "Oktober",
                    "value": "10"
                  },
                  {
                    "label": "November",
                    "value": "11"
                  },
                  {
                    "label": "December",
                    "value": "12"
                  }
                ],
                "validation": {
                  "rules": "required",
                  "messages": {
                    "required": "Dit veld is verplicht"
                  }
                }
              },
              {
                "name": "year",
                "is_required": true,
                "type": "select_input",
                "options": [
                  {
                    "label": "Jaar",
                    "value": "",
                    "selected": true
                  },
                  {
                    "label": "2005",
                    "value": "2005"
                  },
                  {
                    "label": "2004",
                    "value": "2004"
                  },
                  {
                    "label": "2003",
                    "value": "2003"
                  },
                  {
                    "label": "2002",
                    "value": "2002"
                  },
                  {
                    "label": "2001",
                    "value": "2001"
                  },
                  {
                    "label": "2000",
                    "value": "2000"
                  },
                  {
                    "label": "1999",
                    "value": "1999"
                  },
                  {
                    "label": "1998",
                    "value": "1998"
                  },
                  {
                    "label": "1997",
                    "value": "1997"
                  },
                  {
                    "label": "1996",
                    "value": "1996"
                  },
                  {
                    "label": "1995",
                    "value": "1995"
                  },
                  {
                    "label": "1994",
                    "value": "1994"
                  },
                  {
                    "label": "1993",
                    "value": "1993"
                  },
                  {
                    "label": "1992",
                    "value": "1992"
                  },
                  {
                    "label": "1991",
                    "value": "1991"
                  },
                  {
                    "label": "1990",
                    "value": "1990"
                  },
                  {
                    "label": "1989",
                    "value": "1989"
                  },
                  {
                    "label": "1988",
                    "value": "1988"
                  },
                  {
                    "label": "1987",
                    "value": "1987"
                  },
                  {
                    "label": "1986",
                    "value": "1986"
                  },
                  {
                    "label": "1985",
                    "value": "1985"
                  },
                  {
                    "label": "1984",
                    "value": "1984"
                  },
                  {
                    "label": "1983",
                    "value": "1983"
                  },
                  {
                    "label": "1982",
                    "value": "1982"
                  },
                  {
                    "label": "1981",
                    "value": "1981"
                  },
                  {
                    "label": "1980",
                    "value": "1980"
                  },
                  {
                    "label": "1979",
                    "value": "1979"
                  },
                  {
                    "label": "1978",
                    "value": "1978"
                  },
                  {
                    "label": "1977",
                    "value": "1977"
                  },
                  {
                    "label": "1976",
                    "value": "1976"
                  },
                  {
                    "label": "1975",
                    "value": "1975"
                  },
                  {
                    "label": "1974",
                    "value": "1974"
                  },
                  {
                    "label": "1973",
                    "value": "1973"
                  },
                  {
                    "label": "1972",
                    "value": "1972"
                  },
                  {
                    "label": "1971",
                    "value": "1971"
                  },
                  {
                    "label": "1970",
                    "value": "1970"
                  },
                  {
                    "label": "1969",
                    "value": "1969"
                  },
                  {
                    "label": "1968",
                    "value": "1968"
                  },
                  {
                    "label": "1967",
                    "value": "1967"
                  },
                  {
                    "label": "1966",
                    "value": "1966"
                  },
                  {
                    "label": "1965",
                    "value": "1965"
                  },
                  {
                    "label": "1964",
                    "value": "1964"
                  },
                  {
                    "label": "1963",
                    "value": "1963"
                  },
                  {
                    "label": "1962",
                    "value": "1962"
                  },
                  {
                    "label": "1961",
                    "value": "1961"
                  },
                  {
                    "label": "1960",
                    "value": "1960"
                  },
                  {
                    "label": "1959",
                    "value": "1959"
                  },
                  {
                    "label": "1958",
                    "value": "1958"
                  },
                  {
                    "label": "1957",
                    "value": "1957"
                  },
                  {
                    "label": "1956",
                    "value": "1956"
                  },
                  {
                    "label": "1955",
                    "value": "1955"
                  },
                  {
                    "label": "1954",
                    "value": "1954"
                  },
                  {
                    "label": "1953",
                    "value": "1953"
                  },
                  {
                    "label": "1952",
                    "value": "1952"
                  },
                  {
                    "label": "1951",
                    "value": "1951"
                  },
                  {
                    "label": "1950",
                    "value": "1950"
                  },
                  {
                    "label": "1949",
                    "value": "1949"
                  },
                  {
                    "label": "1948",
                    "value": "1948"
                  },
                  {
                    "label": "1947",
                    "value": "1947"
                  },
                  {
                    "label": "1946",
                    "value": "1946"
                  },
                  {
                    "label": "1945",
                    "value": "1945"
                  },
                  {
                    "label": "1944",
                    "value": "1944"
                  },
                  {
                    "label": "1943",
                    "value": "1943"
                  },
                  {
                    "label": "1942",
                    "value": "1942"
                  },
                  {
                    "label": "1941",
                    "value": "1941"
                  },
                  {
                    "label": "1940",
                    "value": "1940"
                  },
                  {
                    "label": "1939",
                    "value": "1939"
                  },
                  {
                    "label": "1938",
                    "value": "1938"
                  },
                  {
                    "label": "1937",
                    "value": "1937"
                  },
                  {
                    "label": "1936",
                    "value": "1936"
                  },
                  {
                    "label": "1935",
                    "value": "1935"
                  },
                  {
                    "label": "1934",
                    "value": "1934"
                  },
                  {
                    "label": "1933",
                    "value": "1933"
                  },
                  {
                    "label": "1932",
                    "value": "1932"
                  },
                  {
                    "label": "1931",
                    "value": "1931"
                  },
                  {
                    "label": "1930",
                    "value": "1930"
                  },
                  {
                    "label": "1929",
                    "value": "1929"
                  },
                  {
                    "label": "1928",
                    "value": "1928"
                  },
                  {
                    "label": "1927",
                    "value": "1927"
                  }
                ],
                "validation": {
                  "rules": "required",
                  "messages": {
                    "required": "Dit veld is verplicht"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "title": "Mijn contactgegevens",
        "fields": [
          {
            "name": "home_address.country_id",
            "label": "Land",
            "type": "select2-input",
            "is_required": true,
            "options": [
              {
                "label": "Albanie",
                "value": "209",
                "sub_fields": "not_nl"
              },
              {
                "label": "Algerije",
                "value": "210",
                "sub_fields": "not_nl"
              },
              {
                "label": "Amerikaans Samoa",
                "value": "1",
                "sub_fields": "not_nl"
              },
              {
                "label": "Andorra",
                "value": "2",
                "sub_fields": "not_nl"
              },
              {
                "label": "Angola",
                "value": "3",
                "sub_fields": "not_nl"
              },
              {
                "label": "Anguilla",
                "value": "4",
                "sub_fields": "not_nl"
              },
              {
                "label": "Antigua",
                "value": "5",
                "sub_fields": "not_nl"
              },
              {
                "label": "Argentinie",
                "value": "6",
                "sub_fields": "not_nl"
              },
              {
                "label": "Armenie",
                "value": "7",
                "sub_fields": "not_nl"
              },
              {
                "label": "Aruba",
                "value": "8",
                "sub_fields": "not_nl"
              },
              {
                "label": "Australie",
                "value": "9",
                "sub_fields": "not_nl"
              },
              {
                "label": "Azerbeidzjan",
                "value": "10",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bahama eilanden",
                "value": "11",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bahrein",
                "value": "12",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bangladesh",
                "value": "13",
                "sub_fields": "not_nl"
              },
              {
                "label": "Barbados",
                "value": "14",
                "sub_fields": "not_nl"
              },
              {
                "label": "Antigua",
                "value": "15",
                "sub_fields": "not_nl"
              },
              {
                "label": "Belgie",
                "value": "16",
                "sub_fields": "not_nl"
              },
              {
                "label": "Belize",
                "value": "17",
                "sub_fields": "not_nl"
              },
              {
                "label": "Benin",
                "value": "18",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bermuda",
                "value": "19",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bhutan",
                "value": "20",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bolivia",
                "value": "21",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bonaire",
                "value": "22",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bosnie-Herzegovina",
                "value": "23",
                "sub_fields": "not_nl"
              },
              {
                "label": "Botswana",
                "value": "24",
                "sub_fields": "not_nl"
              },
              {
                "label": "Brazilie",
                "value": "25",
                "sub_fields": "not_nl"
              },
              {
                "label": "Brunei",
                "value": "26",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bulgarije",
                "value": "27",
                "sub_fields": "not_nl"
              },
              {
                "label": "Burkina Faso",
                "value": "28",
                "sub_fields": "not_nl"
              },
              {
                "label": "Burundi",
                "value": "29",
                "sub_fields": "not_nl"
              },
              {
                "label": "Cambodja",
                "value": "30",
                "sub_fields": "not_nl"
              },
              {
                "label": "Canada",
                "value": "31",
                "sub_fields": "not_nl"
              },
              {
                "label": "Caymaneilanden",
                "value": "32",
                "sub_fields": "not_nl"
              },
              {
                "label": "Chili",
                "value": "33",
                "sub_fields": "not_nl"
              },
              {
                "label": "China",
                "value": "34",
                "sub_fields": "not_nl"
              },
              {
                "label": "Colombia",
                "value": "35",
                "sub_fields": "not_nl"
              },
              {
                "label": "Cookeilanden",
                "value": "36",
                "sub_fields": "not_nl"
              },
              {
                "label": "Costa Rica",
                "value": "37",
                "sub_fields": "not_nl"
              },
              {
                "label": "Cyprus",
                "value": "38",
                "sub_fields": "not_nl"
              },
              {
                "label": "De Solomon-eilanden",
                "value": "39",
                "sub_fields": "not_nl"
              },
              {
                "label": "Democratische Republiek Kongo",
                "value": "40",
                "sub_fields": "not_nl"
              },
              {
                "label": "Denemarken",
                "value": "41",
                "sub_fields": "not_nl"
              },
              {
                "label": "Djibouti",
                "value": "42",
                "sub_fields": "not_nl"
              },
              {
                "label": "Dominica",
                "value": "43",
                "sub_fields": "not_nl"
              },
              {
                "label": "Dominicaanse Republiek",
                "value": "44",
                "sub_fields": "not_nl"
              },
              {
                "label": "Duitsland",
                "value": "45",
                "sub_fields": "not_nl"
              },
              {
                "label": "Ecuador",
                "value": "46",
                "sub_fields": "not_nl"
              },
              {
                "label": "Egypte",
                "value": "47",
                "sub_fields": "not_nl"
              },
              {
                "label": "El Salvador",
                "value": "48",
                "sub_fields": "not_nl"
              },
              {
                "label": "Equatoriaal Guinee",
                "value": "49",
                "sub_fields": "not_nl"
              },
              {
                "label": "Eritrea",
                "value": "50",
                "sub_fields": "not_nl"
              },
              {
                "label": "Estland",
                "value": "51",
                "sub_fields": "not_nl"
              },
              {
                "label": "Ethiopie",
                "value": "52",
                "sub_fields": "not_nl"
              },
              {
                "label": "Fiji",
                "value": "53",
                "sub_fields": "not_nl"
              },
              {
                "label": "Filippijnen",
                "value": "54",
                "sub_fields": "not_nl"
              },
              {
                "label": "Finland",
                "value": "55",
                "sub_fields": "not_nl"
              },
              {
                "label": "Frankrijk",
                "value": "56",
                "sub_fields": "not_nl"
              },
              {
                "label": "Frans-Guyana",
                "value": "57",
                "sub_fields": "not_nl"
              },
              {
                "label": "Frans-Polynesie",
                "value": "58",
                "sub_fields": "not_nl"
              },
              {
                "label": "Gabon",
                "value": "59",
                "sub_fields": "not_nl"
              },
              {
                "label": "Gambia",
                "value": "60",
                "sub_fields": "not_nl"
              },
              {
                "label": "Georgie",
                "value": "61",
                "sub_fields": "not_nl"
              },
              {
                "label": "Ghana",
                "value": "62",
                "sub_fields": "not_nl"
              },
              {
                "label": "Gibraltar",
                "value": "63",
                "sub_fields": "not_nl"
              },
              {
                "label": "Grenada",
                "value": "64",
                "sub_fields": "not_nl"
              },
              {
                "label": "Griekenland",
                "value": "65",
                "sub_fields": "not_nl"
              },
              {
                "label": "Groenland",
                "value": "66",
                "sub_fields": "not_nl"
              },
              {
                "label": "Guadeloupe",
                "value": "67",
                "sub_fields": "not_nl"
              },
              {
                "label": "Guam",
                "value": "68",
                "sub_fields": "not_nl"
              },
              {
                "label": "Guatemala",
                "value": "69",
                "sub_fields": "not_nl"
              },
              {
                "label": "Guinee",
                "value": "70",
                "sub_fields": "not_nl"
              },
              {
                "label": "Guyana",
                "value": "71",
                "sub_fields": "not_nl"
              },
              {
                "label": "Haiti",
                "value": "72",
                "sub_fields": "not_nl"
              },
              {
                "label": "Honduras",
                "value": "73",
                "sub_fields": "not_nl"
              },
              {
                "label": "Hong Kong",
                "value": "74",
                "sub_fields": "not_nl"
              },
              {
                "label": "Hongarije",
                "value": "75",
                "sub_fields": "not_nl"
              },
              {
                "label": "Ierland",
                "value": "76",
                "sub_fields": "not_nl"
              },
              {
                "label": "Ijsland",
                "value": "77",
                "sub_fields": "not_nl"
              },
              {
                "label": "India",
                "value": "78",
                "sub_fields": "not_nl"
              },
              {
                "label": "Indonesie",
                "value": "79",
                "sub_fields": "not_nl"
              },
              {
                "label": "Irak",
                "value": "80",
                "sub_fields": "not_nl"
              },
              {
                "label": "Israel",
                "value": "81",
                "sub_fields": "not_nl"
              },
              {
                "label": "Italie",
                "value": "82",
                "sub_fields": "not_nl"
              },
              {
                "label": "Ivoorkust",
                "value": "83",
                "sub_fields": "not_nl"
              },
              {
                "label": "Jamaica",
                "value": "84",
                "sub_fields": "not_nl"
              },
              {
                "label": "Japan",
                "value": "85",
                "sub_fields": "not_nl"
              },
              {
                "label": "Jemen",
                "value": "86",
                "sub_fields": "not_nl"
              },
              {
                "label": "Jordanie",
                "value": "87",
                "sub_fields": "not_nl"
              },
              {
                "label": "Kaapverdie",
                "value": "88",
                "sub_fields": "not_nl"
              },
              {
                "label": "Kameroen",
                "value": "89",
                "sub_fields": "not_nl"
              },
              {
                "label": "Groot Brittannië",
                "value": "90",
                "sub_fields": "not_nl"
              },
              {
                "label": "Kazachstan",
                "value": "91",
                "sub_fields": "not_nl"
              },
              {
                "label": "Kenia",
                "value": "92",
                "sub_fields": "not_nl"
              },
              {
                "label": "Kirgizie",
                "value": "93",
                "sub_fields": "not_nl"
              },
              {
                "label": "Kiribati",
                "value": "94",
                "sub_fields": "not_nl"
              },
              {
                "label": "Koeweit",
                "value": "95",
                "sub_fields": "not_nl"
              },
              {
                "label": "Democratische Republiek Kongo",
                "value": "96",
                "sub_fields": "not_nl"
              },
              {
                "label": "Korea, Zuid",
                "value": "97",
                "sub_fields": "not_nl"
              },
              {
                "label": "Kroatie",
                "value": "98",
                "sub_fields": "not_nl"
              },
              {
                "label": "Laos",
                "value": "99",
                "sub_fields": "not_nl"
              },
              {
                "label": "Lesotho",
                "value": "100",
                "sub_fields": "not_nl"
              },
              {
                "label": "Letland",
                "value": "101",
                "sub_fields": "not_nl"
              },
              {
                "label": "Libanon",
                "value": "102",
                "sub_fields": "not_nl"
              },
              {
                "label": "Liberia",
                "value": "103",
                "sub_fields": "not_nl"
              },
              {
                "label": "Liechtenstein",
                "value": "104",
                "sub_fields": "not_nl"
              },
              {
                "label": "Litouwen",
                "value": "105",
                "sub_fields": "not_nl"
              },
              {
                "label": "Luxemburg",
                "value": "106",
                "sub_fields": "not_nl"
              },
              {
                "label": "Maagdeneilanden, Amerikaanse",
                "value": "107",
                "sub_fields": "not_nl"
              },
              {
                "label": "Maagdeneilanden, Britse",
                "value": "108",
                "sub_fields": "not_nl"
              },
              {
                "label": "Macau",
                "value": "109",
                "sub_fields": "not_nl"
              },
              {
                "label": "Macedonie",
                "value": "110",
                "sub_fields": "not_nl"
              },
              {
                "label": "Madagascar",
                "value": "111",
                "sub_fields": "not_nl"
              },
              {
                "label": "Malawi",
                "value": "112",
                "sub_fields": "not_nl"
              },
              {
                "label": "Malediven",
                "value": "113",
                "sub_fields": "not_nl"
              },
              {
                "label": "Maleisie",
                "value": "114",
                "sub_fields": "not_nl"
              },
              {
                "label": "Mali",
                "value": "115",
                "sub_fields": "not_nl"
              },
              {
                "label": "Malta",
                "value": "116",
                "sub_fields": "not_nl"
              },
              {
                "label": "Marokko",
                "value": "117",
                "sub_fields": "not_nl"
              },
              {
                "label": "Marshalleilanden",
                "value": "118",
                "sub_fields": "not_nl"
              },
              {
                "label": "Martinique",
                "value": "119",
                "sub_fields": "not_nl"
              },
              {
                "label": "Mauritanie",
                "value": "120",
                "sub_fields": "not_nl"
              },
              {
                "label": "Mauritius",
                "value": "121",
                "sub_fields": "not_nl"
              },
              {
                "label": "Mexico",
                "value": "122",
                "sub_fields": "not_nl"
              },
              {
                "label": "Micronesie",
                "value": "123",
                "sub_fields": "not_nl"
              },
              {
                "label": "Moldavie",
                "value": "124",
                "sub_fields": "not_nl"
              },
              {
                "label": "Monaco",
                "value": "125",
                "sub_fields": "not_nl"
              },
              {
                "label": "Mongolie",
                "value": "126",
                "sub_fields": "not_nl"
              },
              {
                "label": "Montserrat",
                "value": "127",
                "sub_fields": "not_nl"
              },
              {
                "label": "Mozambique",
                "value": "128",
                "sub_fields": "not_nl"
              },
              {
                "label": "Namibie",
                "value": "129",
                "sub_fields": "not_nl"
              },
              {
                "label": "Nauru",
                "value": "130",
                "sub_fields": "not_nl"
              },
              {
                "label": "Nederland",
                "value": "131",
                "selected": true
              },
              {
                "label": "Nederlandse Antillen",
                "value": "132",
                "sub_fields": "not_nl"
              },
              {
                "label": "Nepal",
                "value": "133",
                "sub_fields": "not_nl"
              },
              {
                "label": "Nicaragua",
                "value": "134",
                "sub_fields": "not_nl"
              },
              {
                "label": "Nieuw-Caledonie",
                "value": "135",
                "sub_fields": "not_nl"
              },
              {
                "label": "Nieuw-Zeeland",
                "value": "136",
                "sub_fields": "not_nl"
              },
              {
                "label": "Niger",
                "value": "137",
                "sub_fields": "not_nl"
              },
              {
                "label": "Nigeria",
                "value": "138",
                "sub_fields": "not_nl"
              },
              {
                "label": "Niue",
                "value": "139",
                "sub_fields": "not_nl"
              },
              {
                "label": "Noorwegen",
                "value": "140",
                "sub_fields": "not_nl"
              },
              {
                "label": "Oeganda",
                "value": "141",
                "sub_fields": "not_nl"
              },
              {
                "label": "Oezbekistan",
                "value": "142",
                "sub_fields": "not_nl"
              },
              {
                "label": "Oman",
                "value": "143",
                "sub_fields": "not_nl"
              },
              {
                "label": "Oost-Timor",
                "value": "145",
                "sub_fields": "not_nl"
              },
              {
                "label": "Oostenrijk",
                "value": "144",
                "sub_fields": "not_nl"
              },
              {
                "label": "Pakistan",
                "value": "146",
                "sub_fields": "not_nl"
              },
              {
                "label": "Palau",
                "value": "147",
                "sub_fields": "not_nl"
              },
              {
                "label": "Palestijnse gebieden",
                "value": "148",
                "sub_fields": "not_nl"
              },
              {
                "label": "Panama",
                "value": "149",
                "sub_fields": "not_nl"
              },
              {
                "label": "Papoea-Nieuw-Guinea",
                "value": "150",
                "sub_fields": "not_nl"
              },
              {
                "label": "Paraguay",
                "value": "151",
                "sub_fields": "not_nl"
              },
              {
                "label": "Peru",
                "value": "152",
                "sub_fields": "not_nl"
              },
              {
                "label": "Polen",
                "value": "153",
                "sub_fields": "not_nl"
              },
              {
                "label": "Portugal",
                "value": "154",
                "sub_fields": "not_nl"
              },
              {
                "label": "Puerto Rico",
                "value": "155",
                "sub_fields": "not_nl"
              },
              {
                "label": "Qatar",
                "value": "156",
                "sub_fields": "not_nl"
              },
              {
                "label": "Roemenie",
                "value": "157",
                "sub_fields": "not_nl"
              },
              {
                "label": "Rusland",
                "value": "158",
                "sub_fields": "not_nl"
              },
              {
                "label": "Rwanda",
                "value": "159",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bonaire",
                "value": "160",
                "sub_fields": "not_nl"
              },
              {
                "label": "Saipan",
                "value": "161",
                "sub_fields": "not_nl"
              },
              {
                "label": "San Marino",
                "value": "162",
                "sub_fields": "not_nl"
              },
              {
                "label": "Saudi Arabie",
                "value": "163",
                "sub_fields": "not_nl"
              },
              {
                "label": "Senegal",
                "value": "164",
                "sub_fields": "not_nl"
              },
              {
                "label": "Servie en Montenegro",
                "value": "165",
                "sub_fields": "not_nl"
              },
              {
                "label": "Seychellen",
                "value": "166",
                "sub_fields": "not_nl"
              },
              {
                "label": "Singapore",
                "value": "167",
                "sub_fields": "not_nl"
              },
              {
                "label": "Slovenie",
                "value": "168",
                "sub_fields": "not_nl"
              },
              {
                "label": "Slowakije",
                "value": "169",
                "sub_fields": "not_nl"
              },
              {
                "label": "Spanje",
                "value": "170",
                "sub_fields": "not_nl"
              },
              {
                "label": "Sri Lanka",
                "value": "171",
                "sub_fields": "not_nl"
              },
              {
                "label": "St. Bartholomy",
                "value": "172",
                "sub_fields": "not_nl"
              },
              {
                "label": "Bonaire",
                "value": "173",
                "sub_fields": "not_nl"
              },
              {
                "label": "St. Kitts en Nevis",
                "value": "174",
                "sub_fields": "not_nl"
              },
              {
                "label": "St. Lucia",
                "value": "175",
                "sub_fields": "not_nl"
              },
              {
                "label": "St. Maarten",
                "value": "176",
                "sub_fields": "not_nl"
              },
              {
                "label": "St. Vincent",
                "value": "177",
                "sub_fields": "not_nl"
              },
              {
                "label": "Suriname",
                "value": "178",
                "sub_fields": "not_nl"
              },
              {
                "label": "Swaziland",
                "value": "179",
                "sub_fields": "not_nl"
              },
              {
                "label": "Syrie",
                "value": "180",
                "sub_fields": "not_nl"
              },
              {
                "label": "Taiwan",
                "value": "181",
                "sub_fields": "not_nl"
              },
              {
                "label": "Tanzania",
                "value": "182",
                "sub_fields": "not_nl"
              },
              {
                "label": "Thailand",
                "value": "183",
                "sub_fields": "not_nl"
              },
              {
                "label": "Togo",
                "value": "184",
                "sub_fields": "not_nl"
              },
              {
                "label": "Tonga",
                "value": "185",
                "sub_fields": "not_nl"
              },
              {
                "label": "Trinidad en Tobago",
                "value": "186",
                "sub_fields": "not_nl"
              },
              {
                "label": "Tsjaad",
                "value": "187",
                "sub_fields": "not_nl"
              },
              {
                "label": "Tsjechie",
                "value": "188",
                "sub_fields": "not_nl"
              },
              {
                "label": "Tunesie",
                "value": "189",
                "sub_fields": "not_nl"
              },
              {
                "label": "Turkije",
                "value": "190",
                "sub_fields": "not_nl"
              },
              {
                "label": "Turkmenistan",
                "value": "191",
                "sub_fields": "not_nl"
              },
              {
                "label": "Turkse- en Caicoseilanden",
                "value": "192",
                "sub_fields": "not_nl"
              },
              {
                "label": "Tuvalu",
                "value": "193",
                "sub_fields": "not_nl"
              },
              {
                "label": "Uruguay",
                "value": "194",
                "sub_fields": "not_nl"
              },
              {
                "label": "Vanuatu",
                "value": "195",
                "sub_fields": "not_nl"
              },
              {
                "label": "Vaticaanstad",
                "value": "196",
                "sub_fields": "not_nl"
              },
              {
                "label": "Venezuela",
                "value": "197",
                "sub_fields": "not_nl"
              },
              {
                "label": "Groot Brittannië",
                "value": "198",
                "sub_fields": "not_nl"
              },
              {
                "label": "Verenigde Arabische Emiraten",
                "value": "199",
                "sub_fields": "not_nl"
              },
              {
                "label": "Verenigde Staten van Amerika",
                "value": "200",
                "sub_fields": "not_nl"
              },
              {
                "label": "Vietnam",
                "value": "201",
                "sub_fields": "not_nl"
              },
              {
                "label": "Wallis en Futuna",
                "value": "202",
                "sub_fields": "not_nl"
              },
              {
                "label": "Wit-Rusland",
                "value": "203",
                "sub_fields": "not_nl"
              },
              {
                "label": "Zambia",
                "value": "204",
                "sub_fields": "not_nl"
              },
              {
                "label": "Zimbabwe",
                "value": "205",
                "sub_fields": "not_nl"
              },
              {
                "label": "Zuid-Afrika",
                "value": "206",
                "sub_fields": "not_nl"
              },
              {
                "label": "Zweden",
                "value": "207",
                "sub_fields": "not_nl"
              },
              {
                "label": "Zwitserland",
                "value": "208",
                "sub_fields": "not_nl"
              }
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "Dit veld is verplicht"
              }
            },
            "not_nl": [
              {
                "name": "home_address.street",
                "label": "Straat",
                "type": "text_input",
                "is_required": true,
                "validation": {
                  "rules": "required|min:2",
                  "messages": {
                    "required": "Dit veld is verplicht",
                    "min": "Deze tekst is te kort. Deze moet uit minimaal 2 karakters bestaan."
                  }
                }
              },
              {
                "name": "home_address.city",
                "label": "Plaats",
                "type": "text_input",
                "is_required": true,
                "validation": {
                  "rules": "required|min:2",
                  "messages": {
                    "required": "Dit veld is verplicht",
                    "min": "Deze tekst is te kort. Deze moet uit minimaal 2 karakters bestaan."
                  }
                }
              }
            ]
          },
          {
            "name": "home_address.zipcode",
            "label": "Postcode",
            "type": "text_input",
            "is_required": true,
            "tooltip": "Vul hier je postcode in. Je volledige adres verschijnt automatisch.",
            "placeholder": "bv. 2132WT",
            "validation": {
              "rules": "required|min:6",
              "messages": {
                "required": "Dit veld is verplicht",
                "min": "Deze tekst is te kort. Deze moet uit minimaal 6 karakters bestaan."
              }
            }
          },
          {
            "name": "candidate_home_address_attributes_number",
            "label": "Huisnummer",
            "type": "complex_inputs",
            "is_required": true,
            "placeholder": "huisnr.",
            "options": [
              {
                "name": "home_address.number",
                "type": "text_input",
                "is_required": true,
                "tooltip": "Vul hier je huisnummer in. Je volledige adres verschijnt automatisch. Vul eventueel hier ook een toevoeging op het huisnummer in.",
                "placeholder": "toevoeging",
                "validation": {
                  "rules": "required",
                  "messages": {
                    "required": "Dit veld is verplicht"
                  }
                }
              },
              {
                "name": "home_address.unit",
                "type": "text_input"
              }
            ]
          },
          {
            "name": "telephone",
            "label": "Mobiel / Telefoon",
            "type": "text_input",
            "is_required": true,
            "tooltip": "We bellen alleen als je solliciteert",
            "validation": {
              "rules": "required|numeric",
              "messages": {
                "required": "Dit veld is verplicht",
                "numeric": "Mobiel / Telefoon mag alleen nummers bevatten"
              }
            }
          }
        ]
      },
      {
        "fields": [
          {
            "name": "password",
            "type": "password",
            "label": "Wachtwoord",
            "is_required": true,
            "placeholder": "Wachtwoord",
            "tooltip": "Vul hier je wachtwoord in. Je mag zelf een wachtwoord bedenken. Je wachtwoord moet uit minimaal 4 tekens bestaan, het mag zowel letters als cijfers bevatten. Kies een wachtwoord wat je zelf makkelijk kunt onthouden, maar wat moeilijk te raden is door anderen.",
            "validation": {
              "rules": "required|min:4",
              "messages": {
                "required": "Dit veld is verplicht",
                "min": "Deze tekst is te kort. Deze moet uit minimaal 4 karakters bestaan."
              }
            }
          }
        ],
        "footer_text": "Om te kunnen solliciteren heb je een account nodig. Hiermee kun je in het vervolg met 1 muisklik solliciteren.",
        "footer_class_list": "sub_title"
      },
      {
        "fields": [
          {
            "name": "terms_of_service",
            "type": "agreement",
            "is_required": true,
            "validation": {
              "rules": "required",
              "messages": {
                "required": "Dit veld is verplicht"
              }
            }
          },
          {
            "type": "next_page",
            "label": "Volgende"
          }
        ]
      }
    ],
    "1": [
      {
        "greet_note": "Je aanmelding is bijna voltooid. Binnen 2 minuten kun je direct solliciteren!",
        "show_progress_bar": true,
        "progress_bar": {
          "steps": [
            {
              "title": "Persoonsgegevens"
            },
            {
              "title": "Opleiding & CV"
            },
            {
              "title": "Op zoek naar"
            }
          ],
          "count_text": "Stap 2 van de 3"
        },
        "page_title": "Tweede stap van jouw aanmelding",
        "title": "Wat zijn je opleiding(en)?",
        "fields": [
          {
            "type": "degrees",
            "name": "degrees",
            "buttons": {
              "add": "Voeg extra opleiding toe",
              "remove": "<span class=\"glyphicon glyphicon-remove\"></span>"
            },
            "options": {
              "default": {
                "name": "edu_type_id",
                "type": "select_input",
                "label": "Type",
                "is_required": true,
                "options": [
                  {
                    "label": "",
                    "value": "",
                    "selected": true
                  },
                  {
                    "label": "Middelbare school",
                    "value": 1,
                    "sub_fields": "is_middle_school"
                  },
                  {
                    "label": "MBO",
                    "value": 2,
                    "sub_fields": "is_mbo"
                  },
                  {
                    "label": "HBO",
                    "value": 3,
                    "sub_fields": "is_hbo_uni"
                  },
                  {
                    "label": "Universiteit",
                    "value": 4,
                    "sub_fields": "is_hbo_uni"
                  }
                ],
                "validation": {
                  "rules": "required",
                  "messages": {
                    "required": "Wat zijn je opleiding(en)? moet opgegeven zijn"
                  }
                },
              },
              "is_middle_school": [
                {
                  "name": "edu_level_id",
                  "label": "Niveau",
                  "is_required": true,
                  "type": "select_input",
                  "options": [
                    {
                      "label": "",
                      "value": "",
                      "selected": true
                    },
                    {
                      "label": "VMBO",
                      "value": 46
                    },
                    {
                      "label": "HAVO",
                      "value": 47
                    },
                    {
                      "label": "VWO",
                      "value": 48
                    },
                    {
                      "label": "Anders",
                      "value": 49
                    }
                  ],
                  "validation": {
                    "rules": "required",
                    "messages": {
                      "required": "Niveau moet opgegeven zijn"
                    }
                  }
                }
              ],
              "is_mbo": [
                {
                  "name": "edu_level_id",
                  "label": "Niveau",
                  "is_required": true,
                  "type": "select_input",
                  "options": [
                    {
                      "label": "",
                      "value": "",
                      "selected": true
                    },
                    {
                      "label": "MBO Niveau 1",
                      "value": 66
                    },
                    {
                      "label": "MBO Niveau 2",
                      "value": 67
                    },
                    {
                      "label": "MBO Niveau 3",
                      "value": 68
                    },
                    {
                      "label": "MBO Niveau 4",
                      "value": 69
                    },
                    {
                      "label": "MBO Niveau 5",
                      "value": 70
                    }
                  ],
                  "validation": {
                    "rules": "required",
                    "messages": {
                      "required": "Niveau moet opgegeven zijn"
                    }
                  }
                },
                {
                  "name": "education_id",
                  "label": "Studie",
                  "type": "select_autocomplete",
                  "is_required": true,
                  "options_url": "/educations.json",
                  "field_note": "Staat je studie er niet bij? Kies dan een vergelijkbare.",
                  "validation": {
                    "rules": "required",
                    "messages": {
                      "required": "Studie moet opgegeven zijn"
                    }
                  }
                },
                {
                  "name": "start_date",
                  "type": "date-picker",
                  "label": "Begin Studie",
                  "is_required": true,
                  "options": [
                    {
                      "name": "day",
                      "type": "hidden-input",
                      "value": 1
                    },
                    {
                      "name": "month",
                      "is_required": true,
                      "type": "select_input",
                      "options": [
                        {
                          "label": "Maand",
                          "value": "",
                          "selected": true
                        },
                        {
                          "label": "Januari",
                          "value": "1"
                        },
                        {
                          "label": "Februari",
                          "value": "2"
                        },
                        {
                          "label": "Maart",
                          "value": "3"
                        },
                        {
                          "label": "April",
                          "value": "4"
                        },
                        {
                          "label": "Mei",
                          "value": "5"
                        },
                        {
                          "label": "Juni",
                          "value": "6"
                        },
                        {
                          "label": "Juli",
                          "value": "7"
                        },
                        {
                          "label": "Augustus",
                          "value": "8"
                        },
                        {
                          "label": "September",
                          "value": "9"
                        },
                        {
                          "label": "Oktober",
                          "value": "10"
                        },
                        {
                          "label": "November",
                          "value": "11"
                        },
                        {
                          "label": "December",
                          "value": "12"
                        }
                      ],
                      "validation": {
                        "rules": "required",
                        "messages": {
                          "required": "Maand moet opgegeven zijn"
                        }
                      }
                    },
                    {
                      "name": "year",
                      "is_required": true,
                      "type": "select_input",
                      "options": [
                        {
                          "label": "Jaar",
                          "value": "",
                          "selected": true
                        },
                        {
                          "label": "2027",
                          "value": "2027"
                        },
                        {
                          "label": "2026",
                          "value": "2026"
                        },
                        {
                          "label": "2025",
                          "value": "2025"
                        },
                        {
                          "label": "2024",
                          "value": "2024"
                        },
                        {
                          "label": "2023",
                          "value": "2023"
                        },
                        {
                          "label": "2022",
                          "value": "2022"
                        },
                        {
                          "label": "2021",
                          "value": "2021"
                        },
                        {
                          "label": "2020",
                          "value": "2020"
                        },
                        {
                          "label": "2019",
                          "value": "2019"
                        },
                        {
                          "label": "2018",
                          "value": "2018"
                        },
                        {
                          "label": "2017",
                          "value": "2017"
                        },
                        {
                          "label": "2016",
                          "value": "2016"
                        },
                        {
                          "label": "2015",
                          "value": "2015"
                        },
                        {
                          "label": "2014",
                          "value": "2014"
                        },
                        {
                          "label": "2013",
                          "value": "2013"
                        },
                        {
                          "label": "2012",
                          "value": "2012"
                        },
                        {
                          "label": "2011",
                          "value": "2011"
                        },
                        {
                          "label": "2010",
                          "value": "2010"
                        },
                        {
                          "label": "2009",
                          "value": "2009"
                        },
                        {
                          "label": "2008",
                          "value": "2008"
                        },
                        {
                          "label": "2007",
                          "value": "2007"
                        },
                        {
                          "label": "2006",
                          "value": "2006"
                        },
                        {
                          "label": "2005",
                          "value": "2005"
                        },
                        {
                          "label": "2004",
                          "value": "2004"
                        },
                        {
                          "label": "2003",
                          "value": "2003"
                        },
                        {
                          "label": "2002",
                          "value": "2002"
                        },
                        {
                          "label": "2001",
                          "value": "2001"
                        },
                        {
                          "label": "2000",
                          "value": "2000"
                        },
                        {
                          "label": "1999",
                          "value": "1999"
                        },
                        {
                          "label": "1998",
                          "value": "1998"
                        },
                        {
                          "label": "1997",
                          "value": "1997"
                        },
                        {
                          "label": "1996",
                          "value": "1996"
                        },
                        {
                          "label": "1995",
                          "value": "1995"
                        },
                        {
                          "label": "1994",
                          "value": "1994"
                        },
                        {
                          "label": "1993",
                          "value": "1993"
                        },
                        {
                          "label": "1992",
                          "value": "1992"
                        },
                        {
                          "label": "1991",
                          "value": "1991"
                        },
                        {
                          "label": "1990",
                          "value": "1990"
                        },
                        {
                          "label": "1989",
                          "value": "1989"
                        },
                        {
                          "label": "1988",
                          "value": "1988"
                        },
                        {
                          "label": "1987",
                          "value": "1987"
                        },
                        {
                          "label": "1986",
                          "value": "1986"
                        },
                        {
                          "label": "1985",
                          "value": "1985"
                        },
                        {
                          "label": "1984",
                          "value": "1984"
                        },
                        {
                          "label": "1983",
                          "value": "1983"
                        },
                        {
                          "label": "1982",
                          "value": "1982"
                        },
                        {
                          "label": "1981",
                          "value": "1981"
                        },
                        {
                          "label": "1980",
                          "value": "1980"
                        },
                        {
                          "label": "1979",
                          "value": "1979"
                        },
                        {
                          "label": "1978",
                          "value": "1978"
                        },
                        {
                          "label": "1977",
                          "value": "1977"
                        },
                        {
                          "label": "1976",
                          "value": "1976"
                        },
                        {
                          "label": "1975",
                          "value": "1975"
                        },
                        {
                          "label": "1974",
                          "value": "1974"
                        },
                        {
                          "label": "1973",
                          "value": "1973"
                        },
                        {
                          "label": "1972",
                          "value": "1972"
                        },
                        {
                          "label": "1971",
                          "value": "1971"
                        },
                        {
                          "label": "1970",
                          "value": "1970"
                        },
                        {
                          "label": "1969",
                          "value": "1969"
                        },
                        {
                          "label": "1968",
                          "value": "1968"
                        },
                        {
                          "label": "1967",
                          "value": "1967"
                        }
                      ],
                      "validation": {
                        "rules": "required",
                        "messages": {
                          "required": "Jaar moet opgegeven zijn"
                        }
                      }
                    }
                  ]
                },
                {
                  "name": "end_date",
                  "type": "date-picker",
                  "label": "Eind Studie",
                  "is_required": true,
                  "options": [
                    {
                      "name": "day",
                      "type": "hidden-input",
                      "value": 1
                    },
                    {
                      "name": "month",
                      "is_required": true,
                      "type": "select_input",
                      "options": [
                        {
                          "label": "Maand",
                          "value": "",
                          "selected": true
                        },
                        {
                          "label": "Januari",
                          "value": "1"
                        },
                        {
                          "label": "Februari",
                          "value": "2"
                        },
                        {
                          "label": "Maart",
                          "value": "3"
                        },
                        {
                          "label": "April",
                          "value": "4"
                        },
                        {
                          "label": "Mei",
                          "value": "5"
                        },
                        {
                          "label": "Juni",
                          "value": "6"
                        },
                        {
                          "label": "Juli",
                          "value": "7"
                        },
                        {
                          "label": "Augustus",
                          "value": "8"
                        },
                        {
                          "label": "September",
                          "value": "9"
                        },
                        {
                          "label": "Oktober",
                          "value": "10"
                        },
                        {
                          "label": "November",
                          "value": "11"
                        },
                        {
                          "label": "December",
                          "value": "12"
                        }
                      ],
                      "validation": {
                        "rules": "required",
                        "messages": {
                          "required": "Maand moet opgegeven zijn"
                        }
                      }
                    },
                    {
                      "name": "year",
                      "is_required": true,
                      "type": "select_input",
                      "options": [
                        {
                          "label": "Jaar",
                          "value": "",
                          "selected": true
                        },
                        {
                          "label": "2027",
                          "value": "2027"
                        },
                        {
                          "label": "2026",
                          "value": "2026"
                        },
                        {
                          "label": "2025",
                          "value": "2025"
                        },
                        {
                          "label": "2024",
                          "value": "2024"
                        },
                        {
                          "label": "2023",
                          "value": "2023"
                        },
                        {
                          "label": "2022",
                          "value": "2022"
                        },
                        {
                          "label": "2021",
                          "value": "2021"
                        },
                        {
                          "label": "2020",
                          "value": "2020"
                        },
                        {
                          "label": "2019",
                          "value": "2019"
                        },
                        {
                          "label": "2018",
                          "value": "2018"
                        },
                        {
                          "label": "2017",
                          "value": "2017"
                        },
                        {
                          "label": "2016",
                          "value": "2016"
                        },
                        {
                          "label": "2015",
                          "value": "2015"
                        },
                        {
                          "label": "2014",
                          "value": "2014"
                        },
                        {
                          "label": "2013",
                          "value": "2013"
                        },
                        {
                          "label": "2012",
                          "value": "2012"
                        },
                        {
                          "label": "2011",
                          "value": "2011"
                        },
                        {
                          "label": "2010",
                          "value": "2010"
                        },
                        {
                          "label": "2009",
                          "value": "2009"
                        },
                        {
                          "label": "2008",
                          "value": "2008"
                        },
                        {
                          "label": "2007",
                          "value": "2007"
                        },
                        {
                          "label": "2006",
                          "value": "2006"
                        },
                        {
                          "label": "2005",
                          "value": "2005"
                        },
                        {
                          "label": "2004",
                          "value": "2004"
                        },
                        {
                          "label": "2003",
                          "value": "2003"
                        },
                        {
                          "label": "2002",
                          "value": "2002"
                        },
                        {
                          "label": "2001",
                          "value": "2001"
                        },
                        {
                          "label": "2000",
                          "value": "2000"
                        },
                        {
                          "label": "1999",
                          "value": "1999"
                        },
                        {
                          "label": "1998",
                          "value": "1998"
                        },
                        {
                          "label": "1997",
                          "value": "1997"
                        },
                        {
                          "label": "1996",
                          "value": "1996"
                        },
                        {
                          "label": "1995",
                          "value": "1995"
                        },
                        {
                          "label": "1994",
                          "value": "1994"
                        },
                        {
                          "label": "1993",
                          "value": "1993"
                        },
                        {
                          "label": "1992",
                          "value": "1992"
                        },
                        {
                          "label": "1991",
                          "value": "1991"
                        },
                        {
                          "label": "1990",
                          "value": "1990"
                        },
                        {
                          "label": "1989",
                          "value": "1989"
                        },
                        {
                          "label": "1988",
                          "value": "1988"
                        },
                        {
                          "label": "1987",
                          "value": "1987"
                        },
                        {
                          "label": "1986",
                          "value": "1986"
                        },
                        {
                          "label": "1985",
                          "value": "1985"
                        },
                        {
                          "label": "1984",
                          "value": "1984"
                        },
                        {
                          "label": "1983",
                          "value": "1983"
                        },
                        {
                          "label": "1982",
                          "value": "1982"
                        },
                        {
                          "label": "1981",
                          "value": "1981"
                        },
                        {
                          "label": "1980",
                          "value": "1980"
                        },
                        {
                          "label": "1979",
                          "value": "1979"
                        },
                        {
                          "label": "1978",
                          "value": "1978"
                        },
                        {
                          "label": "1977",
                          "value": "1977"
                        },
                        {
                          "label": "1976",
                          "value": "1976"
                        },
                        {
                          "label": "1975",
                          "value": "1975"
                        },
                        {
                          "label": "1974",
                          "value": "1974"
                        },
                        {
                          "label": "1973",
                          "value": "1973"
                        },
                        {
                          "label": "1972",
                          "value": "1972"
                        },
                        {
                          "label": "1971",
                          "value": "1971"
                        },
                        {
                          "label": "1970",
                          "value": "1970"
                        },
                        {
                          "label": "1969",
                          "value": "1969"
                        },
                        {
                          "label": "1968",
                          "value": "1968"
                        },
                        {
                          "label": "1967",
                          "value": "1967"
                        }
                      ],
                      "validation": {
                        "rules": "required",
                        "messages": {
                          "required": "Jaar moet opgegeven zijn"
                        }
                      }
                    }
                  ]
                }
              ],
              "is_hbo_uni": [
                {
                  "name": "education_id",
                  "label": "Studie",
                  "type": "select_autocomplete",
                  "is_required": true,
                  "options_url": "/educations.json",
                  "field_note": "Staat je studie er niet bij? Kies dan een vergelijkbare.",
                  "validation": {
                    "rules": "required",
                    "messages": {
                      "required": "Studie moet opgegeven zijn"
                    }
                  }
                },
                {
                  "name": "start_date",
                  "type": "date-picker",
                  "label": "Begin Studie",
                  "is_required": true,
                  "options": [
                    {
                      "name": "day",
                      "type": "hidden-input",
                      "value": 1
                    },
                    {
                      "name": "month",
                      "is_required": true,
                      "type": "select_input",
                      "options": [
                        {
                          "label": "Maand",
                          "value": "",
                          "selected": true
                        },
                        {
                          "label": "Januari",
                          "value": "1"
                        },
                        {
                          "label": "Februari",
                          "value": "2"
                        },
                        {
                          "label": "Maart",
                          "value": "3"
                        },
                        {
                          "label": "April",
                          "value": "4"
                        },
                        {
                          "label": "Mei",
                          "value": "5"
                        },
                        {
                          "label": "Juni",
                          "value": "6"
                        },
                        {
                          "label": "Juli",
                          "value": "7"
                        },
                        {
                          "label": "Augustus",
                          "value": "8"
                        },
                        {
                          "label": "September",
                          "value": "9"
                        },
                        {
                          "label": "Oktober",
                          "value": "10"
                        },
                        {
                          "label": "November",
                          "value": "11"
                        },
                        {
                          "label": "December",
                          "value": "12"
                        }
                      ],
                      "validation": {
                        "rules": "required",
                        "messages": {
                          "required": "Maand moet opgegeven zijn"
                        }
                      }
                    },
                    {
                      "name": "year",
                      "is_required": true,
                      "type": "select_input",
                      "options": [
                        {
                          "label": "Jaar",
                          "value": "",
                          "selected": true
                        },
                        {
                          "label": "2027",
                          "value": "2027"
                        },
                        {
                          "label": "2026",
                          "value": "2026"
                        },
                        {
                          "label": "2025",
                          "value": "2025"
                        },
                        {
                          "label": "2024",
                          "value": "2024"
                        },
                        {
                          "label": "2023",
                          "value": "2023"
                        },
                        {
                          "label": "2022",
                          "value": "2022"
                        },
                        {
                          "label": "2021",
                          "value": "2021"
                        },
                        {
                          "label": "2020",
                          "value": "2020"
                        },
                        {
                          "label": "2019",
                          "value": "2019"
                        },
                        {
                          "label": "2018",
                          "value": "2018"
                        },
                        {
                          "label": "2017",
                          "value": "2017"
                        },
                        {
                          "label": "2016",
                          "value": "2016"
                        },
                        {
                          "label": "2015",
                          "value": "2015"
                        },
                        {
                          "label": "2014",
                          "value": "2014"
                        },
                        {
                          "label": "2013",
                          "value": "2013"
                        },
                        {
                          "label": "2012",
                          "value": "2012"
                        },
                        {
                          "label": "2011",
                          "value": "2011"
                        },
                        {
                          "label": "2010",
                          "value": "2010"
                        },
                        {
                          "label": "2009",
                          "value": "2009"
                        },
                        {
                          "label": "2008",
                          "value": "2008"
                        },
                        {
                          "label": "2007",
                          "value": "2007"
                        },
                        {
                          "label": "2006",
                          "value": "2006"
                        },
                        {
                          "label": "2005",
                          "value": "2005"
                        },
                        {
                          "label": "2004",
                          "value": "2004"
                        },
                        {
                          "label": "2003",
                          "value": "2003"
                        },
                        {
                          "label": "2002",
                          "value": "2002"
                        },
                        {
                          "label": "2001",
                          "value": "2001"
                        },
                        {
                          "label": "2000",
                          "value": "2000"
                        },
                        {
                          "label": "1999",
                          "value": "1999"
                        },
                        {
                          "label": "1998",
                          "value": "1998"
                        },
                        {
                          "label": "1997",
                          "value": "1997"
                        },
                        {
                          "label": "1996",
                          "value": "1996"
                        },
                        {
                          "label": "1995",
                          "value": "1995"
                        },
                        {
                          "label": "1994",
                          "value": "1994"
                        },
                        {
                          "label": "1993",
                          "value": "1993"
                        },
                        {
                          "label": "1992",
                          "value": "1992"
                        },
                        {
                          "label": "1991",
                          "value": "1991"
                        },
                        {
                          "label": "1990",
                          "value": "1990"
                        },
                        {
                          "label": "1989",
                          "value": "1989"
                        },
                        {
                          "label": "1988",
                          "value": "1988"
                        },
                        {
                          "label": "1987",
                          "value": "1987"
                        },
                        {
                          "label": "1986",
                          "value": "1986"
                        },
                        {
                          "label": "1985",
                          "value": "1985"
                        },
                        {
                          "label": "1984",
                          "value": "1984"
                        },
                        {
                          "label": "1983",
                          "value": "1983"
                        },
                        {
                          "label": "1982",
                          "value": "1982"
                        },
                        {
                          "label": "1981",
                          "value": "1981"
                        },
                        {
                          "label": "1980",
                          "value": "1980"
                        },
                        {
                          "label": "1979",
                          "value": "1979"
                        },
                        {
                          "label": "1978",
                          "value": "1978"
                        },
                        {
                          "label": "1977",
                          "value": "1977"
                        },
                        {
                          "label": "1976",
                          "value": "1976"
                        },
                        {
                          "label": "1975",
                          "value": "1975"
                        },
                        {
                          "label": "1974",
                          "value": "1974"
                        },
                        {
                          "label": "1973",
                          "value": "1973"
                        },
                        {
                          "label": "1972",
                          "value": "1972"
                        },
                        {
                          "label": "1971",
                          "value": "1971"
                        },
                        {
                          "label": "1970",
                          "value": "1970"
                        },
                        {
                          "label": "1969",
                          "value": "1969"
                        },
                        {
                          "label": "1968",
                          "value": "1968"
                        },
                        {
                          "label": "1967",
                          "value": "1967"
                        }
                      ],
                      "validation": {
                        "rules": "required",
                        "messages": {
                          "required": "Jaar moet opgegeven zijn"
                        }
                      }
                    }
                  ]
                },
                {
                  "name": "end_date",
                  "type": "date-picker",
                  "label": "Eind Studie",
                  "is_required": true,
                  "options": [
                    {
                      "name": "day",
                      "type": "hidden-input",
                      "value": 1
                    },
                    {
                      "name": "month",
                      "is_required": true,
                      "type": "select_input",
                      "options": [
                        {
                          "label": "Maand",
                          "value": "",
                          "selected": true
                        },
                        {
                          "label": "Januari",
                          "value": "1"
                        },
                        {
                          "label": "Februari",
                          "value": "2"
                        },
                        {
                          "label": "Maart",
                          "value": "3"
                        },
                        {
                          "label": "April",
                          "value": "4"
                        },
                        {
                          "label": "Mei",
                          "value": "5"
                        },
                        {
                          "label": "Juni",
                          "value": "6"
                        },
                        {
                          "label": "Juli",
                          "value": "7"
                        },
                        {
                          "label": "Augustus",
                          "value": "8"
                        },
                        {
                          "label": "September",
                          "value": "9"
                        },
                        {
                          "label": "Oktober",
                          "value": "10"
                        },
                        {
                          "label": "November",
                          "value": "11"
                        },
                        {
                          "label": "December",
                          "value": "12"
                        }
                      ],
                      "validation": {
                        "rules": "required",
                        "messages": {
                          "required": "Maand moet opgegeven zijn"
                        }
                      }
                    },
                    {
                      "name": "year",
                      "is_required": true,
                      "type": "select_input",
                      "options": [
                        {
                          "label": "Jaar",
                          "value": "",
                          "selected": true
                        },
                        {
                          "label": "2027",
                          "value": "2027"
                        },
                        {
                          "label": "2026",
                          "value": "2026"
                        },
                        {
                          "label": "2025",
                          "value": "2025"
                        },
                        {
                          "label": "2024",
                          "value": "2024"
                        },
                        {
                          "label": "2023",
                          "value": "2023"
                        },
                        {
                          "label": "2022",
                          "value": "2022"
                        },
                        {
                          "label": "2021",
                          "value": "2021"
                        },
                        {
                          "label": "2020",
                          "value": "2020"
                        },
                        {
                          "label": "2019",
                          "value": "2019"
                        },
                        {
                          "label": "2018",
                          "value": "2018"
                        },
                        {
                          "label": "2017",
                          "value": "2017"
                        },
                        {
                          "label": "2016",
                          "value": "2016"
                        },
                        {
                          "label": "2015",
                          "value": "2015"
                        },
                        {
                          "label": "2014",
                          "value": "2014"
                        },
                        {
                          "label": "2013",
                          "value": "2013"
                        },
                        {
                          "label": "2012",
                          "value": "2012"
                        },
                        {
                          "label": "2011",
                          "value": "2011"
                        },
                        {
                          "label": "2010",
                          "value": "2010"
                        },
                        {
                          "label": "2009",
                          "value": "2009"
                        },
                        {
                          "label": "2008",
                          "value": "2008"
                        },
                        {
                          "label": "2007",
                          "value": "2007"
                        },
                        {
                          "label": "2006",
                          "value": "2006"
                        },
                        {
                          "label": "2005",
                          "value": "2005"
                        },
                        {
                          "label": "2004",
                          "value": "2004"
                        },
                        {
                          "label": "2003",
                          "value": "2003"
                        },
                        {
                          "label": "2002",
                          "value": "2002"
                        },
                        {
                          "label": "2001",
                          "value": "2001"
                        },
                        {
                          "label": "2000",
                          "value": "2000"
                        },
                        {
                          "label": "1999",
                          "value": "1999"
                        },
                        {
                          "label": "1998",
                          "value": "1998"
                        },
                        {
                          "label": "1997",
                          "value": "1997"
                        },
                        {
                          "label": "1996",
                          "value": "1996"
                        },
                        {
                          "label": "1995",
                          "value": "1995"
                        },
                        {
                          "label": "1994",
                          "value": "1994"
                        },
                        {
                          "label": "1993",
                          "value": "1993"
                        },
                        {
                          "label": "1992",
                          "value": "1992"
                        },
                        {
                          "label": "1991",
                          "value": "1991"
                        },
                        {
                          "label": "1990",
                          "value": "1990"
                        },
                        {
                          "label": "1989",
                          "value": "1989"
                        },
                        {
                          "label": "1988",
                          "value": "1988"
                        },
                        {
                          "label": "1987",
                          "value": "1987"
                        },
                        {
                          "label": "1986",
                          "value": "1986"
                        },
                        {
                          "label": "1985",
                          "value": "1985"
                        },
                        {
                          "label": "1984",
                          "value": "1984"
                        },
                        {
                          "label": "1983",
                          "value": "1983"
                        },
                        {
                          "label": "1982",
                          "value": "1982"
                        },
                        {
                          "label": "1981",
                          "value": "1981"
                        },
                        {
                          "label": "1980",
                          "value": "1980"
                        },
                        {
                          "label": "1979",
                          "value": "1979"
                        },
                        {
                          "label": "1978",
                          "value": "1978"
                        },
                        {
                          "label": "1977",
                          "value": "1977"
                        },
                        {
                          "label": "1976",
                          "value": "1976"
                        },
                        {
                          "label": "1975",
                          "value": "1975"
                        },
                        {
                          "label": "1974",
                          "value": "1974"
                        },
                        {
                          "label": "1973",
                          "value": "1973"
                        },
                        {
                          "label": "1972",
                          "value": "1972"
                        },
                        {
                          "label": "1971",
                          "value": "1971"
                        },
                        {
                          "label": "1970",
                          "value": "1970"
                        },
                        {
                          "label": "1969",
                          "value": "1969"
                        },
                        {
                          "label": "1968",
                          "value": "1968"
                        },
                        {
                          "label": "1967",
                          "value": "1967"
                        }
                      ],
                      "validation": {
                        "rules": "required",
                        "messages": {
                          "required": "Jaar moet opgegeven zijn"
                        }
                      }
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "title": "Ben je in het bezit van een rijbewijs?",
        "fields": [
          {
            "name": "has_license",
            "type": "radio",
            "is_required": true,
            "options": [
              {
                "label": "Ja",
                "value": true,
                "sub_fields": "is_driving"
              },
              {
                "label": "Nee",
                "value": false
              }
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "Dit veld is verplicht"
              }
            },
            "is_driving": [
              {
                "name": "has_car",
                "label": "Ben je in het bezit van een auto?",
                "type": "radio",
                "is_required": true,
                "options": [
                  {
                    "label": "Ja",
                    "value": true
                  },
                  {
                    "label": "Nee",
                    "value": false
                  }
                ],
                "validation": {
                  "rules": "required",
                  "messages": {
                    "required": "Dit is verplicht"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "title": "Welke talen beheers je?",
        "fields": [
          {
            "name": "language_ids",
            "type": "checkbox_list",
            "show_first": 4,
            "class_list": "four-columns",
            "is_required": true,
            "options": [
              {
                "label": "Nederlands",
                "value": "27"
              },
              {
                "label": "Engels",
                "value": "1"
              },
              {
                "label": "Duits",
                "value": "2"
              },
              {
                "label": "Frans",
                "value": "3"
              },
              {
                "label": "Arabisch",
                "value": "6"
              },
              {
                "label": "Chinees",
                "value": "7"
              },
              {
                "label": "Deens",
                "value": "8"
              },
              {
                "label": "Fins",
                "value": "9"
              },
              {
                "label": "Fries",
                "value": "35"
              },
              {
                "label": "Grieks",
                "value": "10"
              },
              {
                "label": "Hebreeuws",
                "value": "11"
              },
              {
                "label": "Italiaans",
                "value": "14"
              },
              {
                "label": "Japans",
                "value": "15"
              },
              {
                "label": "Koreaans",
                "value": "16"
              },
              {
                "label": "Latijn",
                "value": "17"
              },
              {
                "label": "Noors",
                "value": "18"
              },
              {
                "label": "Overig",
                "value": "5"
              },
              {
                "label": "Pools",
                "value": "19"
              },
              {
                "label": "Portugees",
                "value": "20"
              },
              {
                "label": "Russisch",
                "value": "21"
              },
              {
                "label": "Spaans",
                "value": "4"
              },
              {
                "label": "Taiwanees",
                "value": "22"
              },
              {
                "label": "Tsjechisch",
                "value": "23"
              },
              {
                "label": "Turks",
                "value": "24"
              },
              {
                "label": "Vlaams",
                "value": "25"
              },
              {
                "label": "Zweeds",
                "value": "26"
              }
            ],
            "selected": [
              "27",
              "1"
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "Dit is verplicht"
              }
            }
          }
        ]
      },
      {
        "title": "Informatie",
        "section_intro": "Wil je gratis tickets en exclusieve aanbiedingen ontvangen op het gebied van events & concerten aangeboden door <strong>IM’IN?</strong>",
        "fields": [
          {
            "name": "partylist_signup",
            "type": "radio",
            "options": [
              {
                "label": "Ja",
                "value": true
              },
              {
                "label": "Nee",
                "value": false
              }
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "Dit is verplicht"
              }
            }
          }
        ]
      },
      {
        "title": "Mijn CV (deze kun je ook later toevoegen)",
        "fields": [
          {
            "type": "cv_section",
            "name": "resume_attributes",
            "options": {
              "default": {
                "name": "resume_thingy",
                "type": "radio",
                "is_required": true,
                "options": [
                  {
                    "value": "",
                    "label": "Ik voeg mijn CV later toe"
                  },
                  {
                    "value": 1,
                    "label": "Ik wil mijn CV uploaden (PDF, Word of RTF bestand)",
                    "sub_fields": "upload_cv"
                  },
                  {
                    "value": 3,
                    "label": "Ik wil mijn LinkedIn profiel toevoegen",
                    "sub_fields": "has_linkedin"
                  },
                  {
                    "value": 2,
                    "label": "Ik wil mijn werkervaring handmatig invoeren",
                    "sub_fields": "work_experiences"
                  }
                ],
                "validation": {
                  "rules": "required",
                  "messages": {
                    "required": "Dit is verplicht"
                  }
                }
              },
              "upload_cv": [
                {
                  "name": "original",
                  "type": "upload-field",
                  "label": "Je CV",
                  "is_required": true,
                  "validation": {
                    "rules": "required|mimes:application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.oasis.opendocument.text,application/vnd.oasis.opendocument.text-template,application/vnd.openofficeorg.extension,application/rtf,text/richtext,application/x-rtf,text/rtf,application/doc",
                    "messages": {
                      "required": "Dit is verplicht",
                      "mimes": "Dit is geen Word, RTF, OpenDocument of PDF bestand"
                    }
                  }
                }
              ],
              "has_linkedin": [
                {
                  "name": "linkedin_profile_link",
                  "label": "Linkedin URL",
                  "type": "text_input",
                  "is_required": true,
                  "validation": {
                    "rules": "required|url",
                    "messages": {
                      "required": "Dit is verplicht",
                      "url": "Lijkt geen geldige LinkedIn url te zijn"
                    }
                  }
                }
              ],
              "work_experiences": [{
                "type": "workexperience",
                "label": "Let op! Geef zoveel mogelijk informatie op. Hoe meer informatie je invult, des te beter is de indruk bij je toekomstige werkgever!",
                "buttons": {
                  "add": "Klik hier om nog een werkervaring toe te voegen",
                  "remove": "<span class=\"glyphicon glyphicon-remove\"></span>"
                },
                "options": [
                  {
                    "name": "company",
                    "label": "Bedrijf",
                    "type": "text_input",
                    "is_required": true,
                    "validation": {
                      "rules": "required",
                      "messages": {
                        "required": "Dit is verplicht"
                      }
                    }
                  },
                  {
                    "name": "function_title",
                    "label": "Functie",
                    "type": "text_input",
                    "is_required": true,
                    "validation": {
                      "rules": "required",
                      "messages": {
                        "required": "Dit is verplicht"
                      }
                    }
                  },
                  {
                    "name": "city",
                    "label": "Vestigingsplaats",
                    "type": "text_input",
                    "is_required": true,
                    "validation": {
                      "rules": "required",
                      "messages": {
                        "required": "Dit is verplicht"
                      }
                    }
                  },
                  {
                    "name": "date_start",
                    "type": "date-picker",
                    "label": "Periode van",
                    "options": [
                      {
                        "name": "month",
                        "is_required": true,
                        "type": "select_input",
                        "options": [
                          {
                            "label": "Maand",
                            "value": "",
                            "selected": true
                          },
                          {
                            "label": "Januari",
                            "value": "1"
                          },
                          {
                            "label": "Februari",
                            "value": "2"
                          },
                          {
                            "label": "Maart",
                            "value": "3"
                          },
                          {
                            "label": "April",
                            "value": "4"
                          },
                          {
                            "label": "Mei",
                            "value": "5"
                          },
                          {
                            "label": "Juni",
                            "value": "6"
                          },
                          {
                            "label": "Juli",
                            "value": "7"
                          },
                          {
                            "label": "Augustus",
                            "value": "8"
                          },
                          {
                            "label": "September",
                            "value": "9"
                          },
                          {
                            "label": "Oktober",
                            "value": "10"
                          },
                          {
                            "label": "November",
                            "value": "11"
                          },
                          {
                            "label": "December",
                            "value": "12"
                          }
                        ],
                        "validation": {
                          "rules": "required",
                          "messages": {
                            "required": "Dit is verplicht"
                          }
                        }
                      },
                      {
                        "name": "year",
                        "is_required": true,
                        "type": "select_input",
                        "options": [
                          {
                            "label": "Jaar",
                            "value": "",
                            "selected": true
                          },
                          {
                            "label": "2027",
                            "value": "2027"
                          },
                          {
                            "label": "2026",
                            "value": "2026"
                          },
                          {
                            "label": "2025",
                            "value": "2025"
                          },
                          {
                            "label": "2024",
                            "value": "2024"
                          },
                          {
                            "label": "2023",
                            "value": "2023"
                          },
                          {
                            "label": "2022",
                            "value": "2022"
                          },
                          {
                            "label": "2021",
                            "value": "2021"
                          },
                          {
                            "label": "2020",
                            "value": "2020"
                          },
                          {
                            "label": "2019",
                            "value": "2019"
                          },
                          {
                            "label": "2018",
                            "value": "2018"
                          },
                          {
                            "label": "2017",
                            "value": "2017"
                          },
                          {
                            "label": "2016",
                            "value": "2016"
                          },
                          {
                            "label": "2015",
                            "value": "2015"
                          },
                          {
                            "label": "2014",
                            "value": "2014"
                          },
                          {
                            "label": "2013",
                            "value": "2013"
                          },
                          {
                            "label": "2012",
                            "value": "2012"
                          },
                          {
                            "label": "2011",
                            "value": "2011"
                          },
                          {
                            "label": "2010",
                            "value": "2010"
                          },
                          {
                            "label": "2009",
                            "value": "2009"
                          },
                          {
                            "label": "2008",
                            "value": "2008"
                          },
                          {
                            "label": "2007",
                            "value": "2007"
                          },
                          {
                            "label": "2006",
                            "value": "2006"
                          },
                          {
                            "label": "2005",
                            "value": "2005"
                          },
                          {
                            "label": "2004",
                            "value": "2004"
                          },
                          {
                            "label": "2003",
                            "value": "2003"
                          },
                          {
                            "label": "2002",
                            "value": "2002"
                          },
                          {
                            "label": "2001",
                            "value": "2001"
                          },
                          {
                            "label": "2000",
                            "value": "2000"
                          },
                          {
                            "label": "1999",
                            "value": "1999"
                          },
                          {
                            "label": "1998",
                            "value": "1998"
                          },
                          {
                            "label": "1997",
                            "value": "1997"
                          },
                          {
                            "label": "1996",
                            "value": "1996"
                          },
                          {
                            "label": "1995",
                            "value": "1995"
                          },
                          {
                            "label": "1994",
                            "value": "1994"
                          },
                          {
                            "label": "1993",
                            "value": "1993"
                          },
                          {
                            "label": "1992",
                            "value": "1992"
                          },
                          {
                            "label": "1991",
                            "value": "1991"
                          },
                          {
                            "label": "1990",
                            "value": "1990"
                          },
                          {
                            "label": "1989",
                            "value": "1989"
                          },
                          {
                            "label": "1988",
                            "value": "1988"
                          },
                          {
                            "label": "1987",
                            "value": "1987"
                          },
                          {
                            "label": "1986",
                            "value": "1986"
                          },
                          {
                            "label": "1985",
                            "value": "1985"
                          },
                          {
                            "label": "1984",
                            "value": "1984"
                          },
                          {
                            "label": "1983",
                            "value": "1983"
                          },
                          {
                            "label": "1982",
                            "value": "1982"
                          },
                          {
                            "label": "1981",
                            "value": "1981"
                          },
                          {
                            "label": "1980",
                            "value": "1980"
                          },
                          {
                            "label": "1979",
                            "value": "1979"
                          },
                          {
                            "label": "1978",
                            "value": "1978"
                          },
                          {
                            "label": "1977",
                            "value": "1977"
                          },
                          {
                            "label": "1976",
                            "value": "1976"
                          },
                          {
                            "label": "1975",
                            "value": "1975"
                          },
                          {
                            "label": "1974",
                            "value": "1974"
                          },
                          {
                            "label": "1973",
                            "value": "1973"
                          },
                          {
                            "label": "1972",
                            "value": "1972"
                          },
                          {
                            "label": "1971",
                            "value": "1971"
                          },
                          {
                            "label": "1970",
                            "value": "1970"
                          },
                          {
                            "label": "1969",
                            "value": "1969"
                          },
                          {
                            "label": "1968",
                            "value": "1968"
                          },
                          {
                            "label": "1967",
                            "value": "1967"
                          }
                        ],
                        "validation": {
                          "rules": "required",
                          "messages": {
                            "required": "Dit is verplicht"
                          }
                        }
                      }
                    ]
                  },
                  {
                    "name": "date_end",
                    "type": "date-picker",
                    "label": "tot",
                    "options": [
                      {
                        "name": "month",
                        "type": "select_input",
                        "options": [
                          {
                            "label": "Maand",
                            "value": "",
                            "selected": true
                          },
                          {
                            "label": "Januari",
                            "value": "1"
                          },
                          {
                            "label": "Februari",
                            "value": "2"
                          },
                          {
                            "label": "Maart",
                            "value": "3"
                          },
                          {
                            "label": "April",
                            "value": "4"
                          },
                          {
                            "label": "Mei",
                            "value": "5"
                          },
                          {
                            "label": "Juni",
                            "value": "6"
                          },
                          {
                            "label": "Juli",
                            "value": "7"
                          },
                          {
                            "label": "Augustus",
                            "value": "8"
                          },
                          {
                            "label": "September",
                            "value": "9"
                          },
                          {
                            "label": "Oktober",
                            "value": "10"
                          },
                          {
                            "label": "November",
                            "value": "11"
                          },
                          {
                            "label": "December",
                            "value": "12"
                          }
                        ],
                        "validation": {
                          "rules": "required",
                          "messages": {
                            "required": "Dit is verplicht"
                          }
                        }
                      },
                      {
                        "name": "year",
                        "type": "select_input",
                        "options": [
                          {
                            "label": "Jaar",
                            "value": "",
                            "selected": true
                          },
                          {
                            "label": "2027",
                            "value": "2027"
                          },
                          {
                            "label": "2026",
                            "value": "2026"
                          },
                          {
                            "label": "2025",
                            "value": "2025"
                          },
                          {
                            "label": "2024",
                            "value": "2024"
                          },
                          {
                            "label": "2023",
                            "value": "2023"
                          },
                          {
                            "label": "2022",
                            "value": "2022"
                          },
                          {
                            "label": "2021",
                            "value": "2021"
                          },
                          {
                            "label": "2020",
                            "value": "2020"
                          },
                          {
                            "label": "2019",
                            "value": "2019"
                          },
                          {
                            "label": "2018",
                            "value": "2018"
                          },
                          {
                            "label": "2017",
                            "value": "2017"
                          },
                          {
                            "label": "2016",
                            "value": "2016"
                          },
                          {
                            "label": "2015",
                            "value": "2015"
                          },
                          {
                            "label": "2014",
                            "value": "2014"
                          },
                          {
                            "label": "2013",
                            "value": "2013"
                          },
                          {
                            "label": "2012",
                            "value": "2012"
                          },
                          {
                            "label": "2011",
                            "value": "2011"
                          },
                          {
                            "label": "2010",
                            "value": "2010"
                          },
                          {
                            "label": "2009",
                            "value": "2009"
                          },
                          {
                            "label": "2008",
                            "value": "2008"
                          },
                          {
                            "label": "2007",
                            "value": "2007"
                          },
                          {
                            "label": "2006",
                            "value": "2006"
                          },
                          {
                            "label": "2005",
                            "value": "2005"
                          },
                          {
                            "label": "2004",
                            "value": "2004"
                          },
                          {
                            "label": "2003",
                            "value": "2003"
                          },
                          {
                            "label": "2002",
                            "value": "2002"
                          },
                          {
                            "label": "2001",
                            "value": "2001"
                          },
                          {
                            "label": "2000",
                            "value": "2000"
                          },
                          {
                            "label": "1999",
                            "value": "1999"
                          },
                          {
                            "label": "1998",
                            "value": "1998"
                          },
                          {
                            "label": "1997",
                            "value": "1997"
                          },
                          {
                            "label": "1996",
                            "value": "1996"
                          },
                          {
                            "label": "1995",
                            "value": "1995"
                          },
                          {
                            "label": "1994",
                            "value": "1994"
                          },
                          {
                            "label": "1993",
                            "value": "1993"
                          },
                          {
                            "label": "1992",
                            "value": "1992"
                          },
                          {
                            "label": "1991",
                            "value": "1991"
                          },
                          {
                            "label": "1990",
                            "value": "1990"
                          },
                          {
                            "label": "1989",
                            "value": "1989"
                          },
                          {
                            "label": "1988",
                            "value": "1988"
                          },
                          {
                            "label": "1987",
                            "value": "1987"
                          },
                          {
                            "label": "1986",
                            "value": "1986"
                          },
                          {
                            "label": "1985",
                            "value": "1985"
                          },
                          {
                            "label": "1984",
                            "value": "1984"
                          },
                          {
                            "label": "1983",
                            "value": "1983"
                          },
                          {
                            "label": "1982",
                            "value": "1982"
                          },
                          {
                            "label": "1981",
                            "value": "1981"
                          },
                          {
                            "label": "1980",
                            "value": "1980"
                          },
                          {
                            "label": "1979",
                            "value": "1979"
                          },
                          {
                            "label": "1978",
                            "value": "1978"
                          },
                          {
                            "label": "1977",
                            "value": "1977"
                          },
                          {
                            "label": "1976",
                            "value": "1976"
                          },
                          {
                            "label": "1975",
                            "value": "1975"
                          },
                          {
                            "label": "1974",
                            "value": "1974"
                          },
                          {
                            "label": "1973",
                            "value": "1973"
                          },
                          {
                            "label": "1972",
                            "value": "1972"
                          },
                          {
                            "label": "1971",
                            "value": "1971"
                          },
                          {
                            "label": "1970",
                            "value": "1970"
                          },
                          {
                            "label": "1969",
                            "value": "1969"
                          },
                          {
                            "label": "1968",
                            "value": "1968"
                          },
                          {
                            "label": "1967",
                            "value": "1967"
                          }
                        ],
                        "validation": {
                          "rules": "required",
                          "messages": {
                            "required": "Dit is verplicht"
                          }
                        }
                      }
                    ],
                    "addCurrent": {
                      "name": "current_job",
                      "type": "checkbox",
                      "value": true,
                      "label": "tot heden"
                    }
                  },
                  {
                    "name": "is_agency",
                    "label": "Via uitzendbureau?",
                    "type": "checkbox_input",
                    "options": [
                      {
                        "label": "",
                        "value": 0
                      }
                    ],
                    "value": 0
                  }
                ]
              }],
            }
          }
        ]
      },
      {
        "fields": [
          {
            "type": "next_page",
            "label": "NAAR LAATSTE STAP"
          }
        ]
      }
    ],
    "2": [
      {
        "show_progress_bar": true,
        "progress_bar": {
          "steps": [
            {
              "title": "Persoonsgegevens"
            },
            {
              "title": "Opleiding & CV"
            },
            {
              "title": "Op zoek naar"
            }
          ],
          "count_text": "Laatste stap: wat zoek je voor werk?"
        },
        "title": "In welk(e) dienstverband(en) wil je het liefst werken?",
        "section_intro": "Kies minimaal één dienstverband",
        "fields": [
          {
            "name": "job_type_ids[]",
            "type": "checkbox_list",
            "is_required": true,
            "class_list": "two-columns",
            "options": [
              {
                "label": "Afstudeerstage",
                "value": "9"
              },
              {
                "label": "Avondwerk",
                "value": "3"
              },
              {
                "label": "Fulltime (ervaren)",
                "value": "7"
              },
              {
                "label": "Fulltime (startersfunctie)",
                "value": "1"
              },
              {
                "label": "Parttime (overdag)",
                "value": "2"
              },
              {
                "label": "Stage",
                "value": "6"
              },
              {
                "label": "Tijdelijke fulltime baan",
                "value": "8"
              },
              {
                "label": "Traineeships",
                "value": "11"
              },
              {
                "label": "Vakantiewerk",
                "value": "5"
              },
              {
                "label": "Vrijwilligerswerk",
                "value": "10"
              },
              {
                "label": "Weekendwerk",
                "value": "4"
              }
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "moet opgegeven zijn"
              }
            }
          }
        ]
      },
      {
        "title": "In welke regio('s) zoek je werk?",
        "fields": [
          {
            "name": "region_ids[]",
            "is_required": true,
            "class_list": "four-columns",
            "type": "checkbox_list",
            "options": [
              {
                "label": "Alkmaar",
                "value": "1"
              },
              {
                "label": "Almelo",
                "value": "42"
              },
              {
                "label": "Almere",
                "value": "43"
              },
              {
                "label": "Alphen aan den Rijn",
                "value": "44"
              },
              {
                "label": "Amersfoort",
                "value": "2"
              },
              {
                "label": "Amsterdam",
                "value": "3"
              },
              {
                "label": "Apeldoorn",
                "value": "4"
              },
              {
                "label": "Arnhem",
                "value": "5"
              },
              {
                "label": "Assen",
                "value": "6"
              },
              {
                "label": "Baarn",
                "value": "45"
              },
              {
                "label": "Bergen op Zoom",
                "value": "7"
              },
              {
                "label": "Breda",
                "value": "8"
              },
              {
                "label": "Culemborg",
                "value": "46"
              },
              {
                "label": "Delft",
                "value": "9"
              },
              {
                "label": "Den Bosch",
                "value": "10"
              },
              {
                "label": "Den Haag",
                "value": "11"
              },
              {
                "label": "Den Helder",
                "value": "73"
              },
              {
                "label": "Deventer",
                "value": "12"
              },
              {
                "label": "Doetinchem",
                "value": "13"
              },
              {
                "label": "Dordrecht",
                "value": "48"
              },
              {
                "label": "Drachten",
                "value": "69"
              },
              {
                "label": "Driebergen",
                "value": "49"
              },
              {
                "label": "Dronten",
                "value": "50"
              },
              {
                "label": "Ede",
                "value": "51"
              },
              {
                "label": "Eindhoven",
                "value": "14"
              },
              {
                "label": "Emmen",
                "value": "15"
              },
              {
                "label": "Enschede",
                "value": "16"
              },
              {
                "label": "Gorinchem",
                "value": "17"
              },
              {
                "label": "Goes",
                "value": "74"
              },
              {
                "label": "Gouda",
                "value": "18"
              },
              {
                "label": "Groningen",
                "value": "19"
              },
              {
                "label": "Haarlem",
                "value": "20"
              },
              {
                "label": "Harderwijk",
                "value": "72"
              },
              {
                "label": "Heerenveen",
                "value": "21"
              },
              {
                "label": "Heerlen",
                "value": "22"
              },
              {
                "label": "Helmond",
                "value": "23"
              },
              {
                "label": "Hengelo",
                "value": "52"
              },
              {
                "label": "Hilversum",
                "value": "53"
              },
              {
                "label": "Hoofddorp",
                "value": "54"
              },
              {
                "label": "Hoogeveen",
                "value": "68"
              },
              {
                "label": "Hoorn",
                "value": "47"
              },
              {
                "label": "Kampen",
                "value": "55"
              },
              {
                "label": "Leeuwarden",
                "value": "24"
              },
              {
                "label": "Leiden",
                "value": "25"
              },
              {
                "label": "Lelystad",
                "value": "26"
              },
              {
                "label": "Maastricht",
                "value": "27"
              },
              {
                "label": "Meppel",
                "value": "76"
              },
              {
                "label": "Middelburg",
                "value": "75"
              },
              {
                "label": "Naaldwijk",
                "value": "62"
              },
              {
                "label": "Nijmegen",
                "value": "28"
              },
              {
                "label": "Oosterhout",
                "value": "70"
              },
              {
                "label": "Oss",
                "value": "71"
              },
              {
                "label": "Rijswijk",
                "value": "56"
              },
              {
                "label": "Roermond",
                "value": "29"
              },
              {
                "label": "Roosendaal",
                "value": "63"
              },
              {
                "label": "Rotterdam",
                "value": "30"
              },
              {
                "label": "Schiphol",
                "value": "67"
              },
              {
                "label": "Sittard",
                "value": "57"
              },
              {
                "label": "Tilburg",
                "value": "31"
              },
              {
                "label": "Uden",
                "value": "65"
              },
              {
                "label": "Utrecht",
                "value": "32"
              },
              {
                "label": "Veenendaal",
                "value": "60"
              },
              {
                "label": "Velp",
                "value": "58"
              },
              {
                "label": "Venlo",
                "value": "33"
              },
              {
                "label": "Vlissingen",
                "value": "34"
              },
              {
                "label": "Waalwijk",
                "value": "64"
              },
              {
                "label": "Wageningen",
                "value": "35"
              },
              {
                "label": "Zaandam",
                "value": "59"
              },
              {
                "label": "Zoetermeer",
                "value": "66"
              },
              {
                "label": "Zutphen",
                "value": "61"
              },
              {
                "label": "Zwolle",
                "value": "36"
              },
              {
                "label": "Afrika",
                "value": "37"
              },
              {
                "label": "Amerika",
                "value": "38"
              },
              {
                "label": "Australie",
                "value": "40"
              },
              {
                "label": "Azie",
                "value": "39"
              },
              {
                "label": "Europa",
                "value": "41"
              }
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "moet opgegeven zijn"
              }
            }
          }
        ]
      },
      {
        "title": "Wat voor soort werk zou je willen doen?",
        "section_intro": "Kies minimaal één functiegroep",
        "fields": [
          {
            "name": "function_ids[]",
            "type": "checkbox_list",
            "class_list": "two-columns",
            "is_required": true,
            "options": [
              {
                "label": "Administratief\n",
                "value": "1",
                "tooltip": "Administratief medewerk(st)er Data-entry medewerk(st)er Archiefmedewerk(st)er"
              },
              {
                "label": "Agrarisch\n",
                "value": "2",
                "tooltip": "Landbouwmedewerk(st)er Tuinbouwmedewerk(st)er Veeteelt medewerk(st)er Hovenier Bloemenkweker"
              },
              {
                "label": "Artistiek / Creatief / Ambachtelijk\n",
                "value": "3",
                "tooltip": "Ambachtslieden, Ontwerper, Fotograaf, Schilder"
              },
              {
                "label": "Beveiliging\n",
                "value": "36",
                "tooltip": "Beveiliger, Portier, Toezichthouder"
              },
              {
                "label": "Bouw / Civiel technisch\n",
                "value": "4",
                "tooltip": "Ingenieur, Architect, Werkvoorbereider, Bouwkundige"
              },
              {
                "label": "Callcenter / Customer Service\n",
                "value": "34",
                "tooltip": "Callcenter medewerk(st)er Telefonisch verkoper Telemarketeer Klantenservicemedewerk(st)er Helpdeskmedewerk(st)er (telefonisch)"
              },
              {
                "label": "Chemisch / Farmaceutisch\n",
                "value": "5",
                "tooltip": "Apotheker, apothekersassistente, Laborant, Chemicus "
              },
              {
                "label": "Commercieel / Verkoop / Inkoop\n",
                "value": "31",
                "tooltip": "Verkoper Binnendienstmedewerk(st)er Accountmanager Inkoopmedewerk(st)er"
              },
              {
                "label": "Communicatie / Marketing / Reclame / PR\n",
                "value": "6",
                "tooltip": "(online) Marketeer, PR-medewerk(st)er, Communicatiemedewerk(st)er "
              },
              {
                "label": "Consultancy / Beleid\n",
                "value": "22",
                "tooltip": "Adviseur/Consultant Beleidsmedewerk(st)er, Business analist"
              },
              {
                "label": "Elektrotechnisch / Electronica\n",
                "value": "7",
                "tooltip": "Elektromonteur, Servicemonteur, Onderhoudsmedewerk(st)er "
              },
              {
                "label": "Financieel / Accountancy\n",
                "value": "27",
                "tooltip": ""
              },
              {
                "label": "Horeca / Catering\n",
                "value": "10",
                "tooltip": "Barpersoneel Cateringmedewerk(st)er Afwashulp, Bediening, (Chef) Kok "
              },
              {
                "label": "ICT / IT / Programmeur\n",
                "value": "11",
                "tooltip": "Programmeur, Systeembeheer, Software specialist, Ontwikkelaar, Tester "
              },
              {
                "label": "Juridisch / Bestuurlijk / Ambtelijk\n",
                "value": "13",
                "tooltip": "Juridisch medewerk(st)er, Advocaat, Jurist "
              },
              {
                "label": "Management / Leidinggevend\n",
                "value": "28",
                "tooltip": "Manager, Winkelmanager, Bedrijfsleider, Supervisor, Directeur, Teamleider "
              },
              {
                "label": "Medisch\n",
                "value": "16",
                "tooltip": "Arts, Tandarts, (Fysio)Therapeut(e), Verloskundige, Dokters-assistente "
              },
              {
                "label": "Metaaltechnisch / Werktuigbouwkundig\n",
                "value": "17",
                "tooltip": "Werktuigbouwkundige Metaalbewerker/Lasser"
              },
              {
                "label": "Onderwijs en Wetenschap\n",
                "value": "18",
                "tooltip": "Docent/Leraar/Onderwijzer Hoogleraar, Onderzoeker, Onderwijs-assistent(e) "
              },
              {
                "label": "Online Marketing\n",
                "value": "39",
                "tooltip": "SEO specialist, SEA specialist, CRO specialist, Analytics specialist, Copywriter"
              },
              {
                "label": "Personeel & Organisatie / HRM\n",
                "value": "19",
                "tooltip": "P&O medewerk(st)er, Recruiter/Intercedent, HR adviseur "
              },
              {
                "label": "Productie\n",
                "value": "20",
                "tooltip": "Productiemedewerk(st)er Productieplanner "
              },
              {
                "label": "Projectmanagement\n",
                "value": "30",
                "tooltip": "Projectleider Projectmedewerk(st)er Projectmanager "
              },
              {
                "label": "Promotiewerk / Hostess\n",
                "value": "37",
                "tooltip": "Gastheer/vrouw Promotiemedewerk(st)er "
              },
              {
                "label": "Receptionist(e) / Telefonist(e)\n",
                "value": "33",
                "tooltip": "Receptioniste, Frontoffice medewerk(st)er, Baliemedewerk(st)er "
              },
              {
                "label": "Recreatief / Sport / Toerisme\n",
                "value": "8",
                "tooltip": "Animatiemedewerk(st)er, Reisleider, Sport instructeur "
              },
              {
                "label": "Schoonmaak / Facilitair\n",
                "value": "35",
                "tooltip": "Schoonmaker, Facilitair medewerk(st)er, Huismeester, Conciërge, Kamermeisje"
              },
              {
                "label": "Secretarieel\n",
                "value": "21",
                "tooltip": "Secretaresse/Secretaris"
              },
              {
                "label": "Technisch / Klusser / Monteur\n",
                "value": "23",
                "tooltip": "Monteur, Loodgieter, Klusjesman/Handyman, Technicus"
              },
              {
                "label": "Training / Opleiding\n",
                "value": "32",
                "tooltip": "Trainer, Coach, Instructeur, Opleidingsadviseur"
              },
              {
                "label": "Transport / Logistiek / Chauffeur / Koerier\n",
                "value": "14",
                "tooltip": "Logistiekmedewerk(st)er Orderpicker Chauffeur/Koerier/Hiker Magazijnmedewerk(st)er Postbode "
              },
              {
                "label": "Uiterlijke verzorging\n",
                "value": "25",
                "tooltip": "Kapper, Schoonheidsspecialiste, Masseur, Visagist, Manicure/Pedicure"
              },
              {
                "label": "Winkelwerk / Retail / Detailhandel\n",
                "value": "38",
                "tooltip": "Winkelmedewerk(st)er Vakkenvuller Kassamedewerk(st)er "
              },
              {
                "label": "Zorg / Thuiszorg / Kinderopvang\n",
                "value": "26",
                "tooltip": "Verpleegkundige, Au Pair, Thuiszorgmedewerk(st)er, Kraamverzorgster, Kinderleidster"
              }
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "moet opgegeven zijn"
              }
            }
          }
        ]
      },
      {
        "title": "Zou je ook als freelancer willen werken?",
        "section_intro": "Indien je nog geen KvK-registratie hebt en je bent bereid deze wel aan te vragen dan kun  je hier ook ja antwoorden.",
        "fields": [
          {
            "name": "freelancer",
            "type": "radio",
            "is_required": true,
            "options": [
              {
                "value": true,
                "label": "Ja",
                "sub_fields": "is_freelancer"
              },
              {
                "value": false,
                "label": "Nee"
              }
            ],
            "validation": {
              "rules": "required",
              "messages": {
                "required": "Dit is verplicht"
              }
            },
            "is_freelancer": [
              {
                "type": "radio",
                "name": "has_coc_number",
                "label": "Heb je al een KvK-nummer?",
                "is_required": true,
                "options": [
                  {
                    "value": true,
                    "label": "Ja"
                  },
                  {
                    "value": false,
                    "label": "Nee"
                  }
                ],
                "validation": {
                  "rules": "required",
                  "messages": {
                    "required": "Dit is verplicht"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "fields": [
          {
            "type": "next_page",
            "label": "Gegevens opslaan"
          }
        ]
      }
    ]
  }
}
export default Config
